#!/bin/bash



regime_mode=$(zenity --width=570 --height=220 --list \
                    --title="Имя БД, из которой будут выгружены классификаторы" \
                    --column="" --column="" \
                      1 "Выгрузка из базы данных sodo_test" \
                      2 "Выгрузка из базы данных sodo" \
                      3 "Выгрузка из базы данных sodo_uch" \
                      4 "Выгрузка из базы данных sodo_test_uch" \
                )
regime_mode_choice=$?                
if [[ $regime_mode_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi





#-------------------------------------------------------------------------------------------------------------------------------------------------------------
# Ввод данных 


host_address_name=$(zenity --width=200 --height=120 --entry --title="" --text="Введите адрес хоста")
host_address_name_choice=$?
if [[ $host_address_name_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi

base_user_name=$(zenity --width=200 --height=120 --entry --title="" --text='Введите имя пользователя')
user_base_name_choice=$?
if [[ $user_base_name_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi

password_base=$(zenity --width=200 --height=120 --entry --title="" --text='Введите пароль')
password_base_choice=$?
if [[ $password_base_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi

host_port=$(zenity --width=200 --height=120 --entry --title="" --text='Введите порт')
host_port_choice=$?
if [[ $host_port_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi
#-------------------------------------------------------------------------------------------------------------------------------------------------------------







# ТЕСТОВЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 1)); then

    array_of_save_scen_files=( "scen_save_cl_garnizon_test.xml" "scen_save_cl_gs_test.xml"  "scen_save_cl_mob_events_test.xml" "scen_save_cl_okrug_flot_test.xml"  "scen_save_cl_osoben_test.xml"  "scen_save_cl_pd_test.xml" "scen_save_cl_posts_gp_test.xml" "scen_save_cl_posts_off_test.xml" "scen_save_cl_posts_sold_test.xml" "scen_save_cl_ppv_gd_test.xml" "scen_save_cl_prednaz_test.xml"  "scen_save_cl_rod_vs_test.xml" "scen_save_cl_ship_types_test.xml" "scen_save_cl_specialization_test.xml" "scen_save_cl_state_names_test.xml" "scen_save_cl_vid_vs_test.xml" "scen_save_cl_vvt_types_test.xml")
    
    
    
    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_save_scen_files[@]}; do

        # ищем тег с db_name в сценарии
        host_value=$(grep "<$host_name>.*<.$host_name>" $xml_file | sed -e "s/^.*<$host_name/<$host_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_user в сценарии
        user_value=$(grep "<$user_name>.*<.$user_name>" $xml_file | sed -e "s/^.*<$user_name/<$user_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_pass в сценарии
        password_value=$(grep "<$password>.*<.$password>" $xml_file | sed -e "s/^.*<$password/<$password/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_port в сценарии
        port_value=$(grep "<$port>.*<.$port>" $xml_file | sed -e "s/^.*<$port/<$port/" | cut -f2 -d">"| cut -f1 -d"<")
        
        # переписываем значение внутри тега db_host в сценарии
        sed -e "s/<$host_name>$host_value<\/$host_name>/<$host_name>$host_address_name<\/$host_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_user в сценарии
        sed -e "s/<$user_name>$user_value<\/$user_name>/<$user_name>$base_user_name<\/$user_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_pass в сценарии
        sed -e "s/<$password>$password_value<\/$password>/<$password>$password_base<\/$password>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_port в сценарии
        sed -e "s/<$port>$port_value<\/$port>/<$port>$host_port<\/$port>/g" $xml_file > $temporary
        mv $temporary $xml_file
    done
        
        
    zenity --question --width=300 --height=120 \
        --text="Начать выгрузку классификаторов в формате ИМОД из базы данных sodo_test?" \
        --ok-label="Да" \
        --cancel-label="Нет"

    download_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $download_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_saver () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс выгрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }
    
    
        ./classsaver -ng -s scen_save_cl_vvt_types_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_saver "Выгрузка Классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'"
        
        ./classsaver -ng -s scen_save_cl_ship_types_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Типы кораблей'.xml" & progress_saver "Выгрузка Классификатора 'Типы кораблей'"
        
        ./classsaver -ng -s scen_save_cl_garnizon_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень гарнизонов'.xml" & progress_saver "Выгрузка Классификатора 'Перечень гарнизонов'"
        
        ./classsaver -ng -s scen_save_cl_gs_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименований главных слов'.xml" & progress_saver "Выгрузка Классификатора 'Наименований главных слов'"
        
        ./classsaver -ng -s scen_save_cl_mob_events_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_saver "Выгрузка Классификатора 'Мобилизационные мероприятия'"
        
        ./classsaver -ng -s scen_save_cl_okrug_flot_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_saver "Выгрузка Классификатора 'Перечень военных округов, флотов, флотилий'"
        
        ./classsaver -ng -s scen_save_cl_osoben_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования особенностей'.xml" & progress_saver "Выгрузка Классификатора 'Наименования особенностей'"
        
        ./classsaver -ng -s scen_save_cl_pd_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Пункты дислокации'.xml" & progress_saver "Выгрузка Классификатора 'Пункты дислокации'"
        
        ./classsaver -ng -s scen_save_cl_posts_gp_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей гражданского персонала'"
        
        ./classsaver -ng -s scen_save_cl_posts_off_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей офицеров'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей офицеров'"
        
        ./classsaver -ng -s scen_save_cl_posts_sold_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей солдат'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей солдат'"
        
        ./classsaver -ng -s scen_save_cl_ppv_gd_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_saver "Выгрузка Классификатора 'Пунктов погрузки-выгрузки'"
        
        ./classsaver -ng -s scen_save_cl_prednaz_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_saver "Выгрузка Классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'"
        
        ./classsaver -ng -s scen_save_cl_rod_vs_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_saver "Выгрузка Классификатора 'Перечень родов войск, специальных войск и служб'"
        
        ./classsaver -ng -s scen_save_cl_vid_vs_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_saver "Выгрузка Классификатора 'Перечень видов Вооруженных Сил'"
        
        ./classsaver -ng -s scen_save_cl_specialization_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования специализации'.xml" & progress_saver "Выгрузка Классификатора 'Наименования специализации'"
        
        ./classsaver -ng -s scen_save_cl_state_names_test.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_saver "Выгрузка Классификатора 'Наименования штатов и структурных подразделений'"
    fi
fi








# БОЕВЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 2)); then

    array_of_save_scen_files=( "scen_save_cl_garnizon_battle.xml" "scen_save_cl_gs_battle.xml"  "scen_save_cl_mob_events_battle.xml" "scen_save_cl_okrug_flot_battle.xml"  "scen_save_cl_osoben_battle.xml"  "scen_save_cl_pd_battle.xml" "scen_save_cl_posts_gp_battle.xml" "scen_save_cl_posts_off_battle.xml" "scen_save_cl_posts_sold_battle.xml" "scen_save_cl_ppv_gd_battle.xml" "scen_save_cl_prednaz_battle.xml"  "scen_save_cl_rod_vs_battle.xml" "scen_save_cl_ship_types_battle.xml" "scen_save_cl_specialization_battle.xml" "scen_save_cl_state_names_battle.xml" "scen_save_cl_vid_vs_battle.xml" "scen_save_cl_vvt_types_battle.xml")
    
    
    
    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_save_scen_files[@]}; do

        # ищем тег с db_name в сценарии
        host_value=$(grep "<$host_name>.*<.$host_name>" $xml_file | sed -e "s/^.*<$host_name/<$host_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_user в сценарии
        user_value=$(grep "<$user_name>.*<.$user_name>" $xml_file | sed -e "s/^.*<$user_name/<$user_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_pass в сценарии
        password_value=$(grep "<$password>.*<.$password>" $xml_file | sed -e "s/^.*<$password/<$password/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_port в сценарии
        port_value=$(grep "<$port>.*<.$port>" $xml_file | sed -e "s/^.*<$port/<$port/" | cut -f2 -d">"| cut -f1 -d"<")
        
        # переписываем значение внутри тега db_host в сценарии
        sed -e "s/<$host_name>$host_value<\/$host_name>/<$host_name>$host_address_name<\/$host_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_user в сценарии
        sed -e "s/<$user_name>$user_value<\/$user_name>/<$user_name>$base_user_name<\/$user_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_pass в сценарии
        sed -e "s/<$password>$password_value<\/$password>/<$password>$password_base<\/$password>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_port в сценарии
        sed -e "s/<$port>$port_value<\/$port>/<$port>$host_port<\/$port>/g" $xml_file > $temporary
        mv $temporary $xml_file
    done
        
        
    zenity --question --width=300 --height=120 \
        --text="Начать выгрузку классификаторов в формате ИМОД из базы данных sodo?" \
        --ok-label="Да" \
        --cancel-label="Нет"

    download_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $download_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_saver () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс выгрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }
    
    
        ./classsaver -ng -s scen_save_cl_vvt_types_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_saver "Выгрузка Классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'"
        
        ./classsaver -ng -s scen_save_cl_ship_types_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Типы кораблей'.xml" & progress_saver "Выгрузка Классификатора 'Типы кораблей'"
        
        ./classsaver -ng -s scen_save_cl_garnizon_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень гарнизонов'.xml" & progress_saver "Выгрузка Классификатора 'Перечень гарнизонов'"
        
        ./classsaver -ng -s scen_save_cl_gs_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименований главных слов'.xml" & progress_saver "Выгрузка Классификатора 'Наименований главных слов'"
        
        ./classsaver -ng -s scen_save_cl_mob_events_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_saver "Выгрузка Классификатора 'Мобилизационные мероприятия'"
        
        ./classsaver -ng -s scen_save_cl_okrug_flot_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_saver "Выгрузка Классификатора 'Перечень военных округов, флотов, флотилий'"
        
        ./classsaver -ng -s scen_save_cl_osoben_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования особенностей'.xml" & progress_saver "Выгрузка Классификатора 'Наименования особенностей'"
        
        ./classsaver -ng -s scen_save_cl_pd_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Пункты дислокации'.xml" & progress_saver "Выгрузка Классификатора 'Пункты дислокации'"
        
        ./classsaver -ng -s scen_save_cl_posts_gp_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей гражданского персонала'"
        
        ./classsaver -ng -s scen_save_cl_posts_off_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей офицеров'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей офицеров'"
        
        ./classsaver -ng -s scen_save_cl_posts_sold_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей солдат'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей солдат'"
        
        ./classsaver -ng -s scen_save_cl_ppv_gd_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_saver "Выгрузка Классификатора 'Пунктов погрузки-выгрузки'"
        
        ./classsaver -ng -s scen_save_cl_prednaz_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_saver "Выгрузка Классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'"
        
        ./classsaver -ng -s scen_save_cl_rod_vs_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_saver "Выгрузка Классификатора 'Перечень родов войск, специальных войск и служб'"
        
        ./classsaver -ng -s scen_save_cl_vid_vs_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_saver "Выгрузка Классификатора 'Перечень видов Вооруженных Сил'"
        
        ./classsaver -ng -s scen_save_cl_specialization_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования специализации'.xml" & progress_saver "Выгрузка Классификатора 'Наименования специализации'"
        
        ./classsaver -ng -s scen_save_cl_state_names_battle.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_saver "Выгрузка Классификатора 'Наименования штатов и структурных подразделений'"
    fi
fi


















# УЧЕБНЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 3)); then

    array_of_save_scen_files=( "scen_save_cl_garnizon_uch.xml" "scen_save_cl_gs_uch.xml"  "scen_save_cl_mob_events_uch.xml" "scen_save_cl_okrug_flot_uch.xml"  "scen_save_cl_osoben_uch.xml"  "scen_save_cl_pd_uch.xml" "scen_save_cl_posts_gp_uch.xml" "scen_save_cl_posts_off_uch.xml" "scen_save_cl_posts_sold_uch.xml" "scen_save_cl_ppv_gd_uch.xml" "scen_save_cl_prednaz_uch.xml"  "scen_save_cl_rod_vs_uch.xml" "scen_save_cl_ship_types_uch.xml" "scen_save_cl_specialization_uch.xml" "scen_save_cl_state_names_uch.xml" "scen_save_cl_vid_vs_uch.xml" "scen_save_cl_vvt_types_uch.xml")
    
    
    
    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_save_scen_files[@]}; do

        # ищем тег с db_name в сценарии
        host_value=$(grep "<$host_name>.*<.$host_name>" $xml_file | sed -e "s/^.*<$host_name/<$host_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_user в сценарии
        user_value=$(grep "<$user_name>.*<.$user_name>" $xml_file | sed -e "s/^.*<$user_name/<$user_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_pass в сценарии
        password_value=$(grep "<$password>.*<.$password>" $xml_file | sed -e "s/^.*<$password/<$password/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_port в сценарии
        port_value=$(grep "<$port>.*<.$port>" $xml_file | sed -e "s/^.*<$port/<$port/" | cut -f2 -d">"| cut -f1 -d"<")
        
        # переписываем значение внутри тега db_host в сценарии
        sed -e "s/<$host_name>$host_value<\/$host_name>/<$host_name>$host_address_name<\/$host_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_user в сценарии
        sed -e "s/<$user_name>$user_value<\/$user_name>/<$user_name>$base_user_name<\/$user_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_pass в сценарии
        sed -e "s/<$password>$password_value<\/$password>/<$password>$password_base<\/$password>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_port в сценарии
        sed -e "s/<$port>$port_value<\/$port>/<$port>$host_port<\/$port>/g" $xml_file > $temporary
        mv $temporary $xml_file
    done
        
        
    zenity --question --width=300 --height=120 \
        --text="Начать выгрузку классификаторов в формате ИМОД из базы данных sodo_uch?" \
        --ok-label="Да" \
        --cancel-label="Нет"

    download_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $download_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_saver () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс выгрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }
    
    
        ./classsaver -ng -s scen_save_cl_vvt_types_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_saver "Выгрузка Классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'"
        
        ./classsaver -ng -s scen_save_cl_ship_types_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Типы кораблей'.xml" & progress_saver "Выгрузка Классификатора 'Типы кораблей'"
        
        ./classsaver -ng -s scen_save_cl_garnizon_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень гарнизонов'.xml" & progress_saver "Выгрузка Классификатора 'Перечень гарнизонов'"
        
        ./classsaver -ng -s scen_save_cl_gs_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименований главных слов'.xml" & progress_saver "Выгрузка Классификатора 'Наименований главных слов'"
        
        ./classsaver -ng -s scen_save_cl_mob_events_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_saver "Выгрузка Классификатора 'Мобилизационные мероприятия'"
        
        ./classsaver -ng -s scen_save_cl_okrug_flot_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_saver "Выгрузка Классификатора 'Перечень военных округов, флотов, флотилий'"
        
        ./classsaver -ng -s scen_save_cl_osoben_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования особенностей'.xml" & progress_saver "Выгрузка Классификатора 'Наименования особенностей'"
        
        ./classsaver -ng -s scen_save_cl_pd_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Пункты дислокации'.xml" & progress_saver "Выгрузка Классификатора 'Пункты дислокации'"
        
        ./classsaver -ng -s scen_save_cl_posts_gp_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей гражданского персонала'"
        
        ./classsaver -ng -s scen_save_cl_posts_off_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей офицеров'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей офицеров'"
        
        ./classsaver -ng -s scen_save_cl_posts_sold_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей солдат'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей солдат'"
        
        ./classsaver -ng -s scen_save_cl_ppv_gd_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_saver "Выгрузка Классификатора 'Пунктов погрузки-выгрузки'"
        
        ./classsaver -ng -s scen_save_cl_prednaz_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_saver "Выгрузка Классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'"
        
        ./classsaver -ng -s scen_save_cl_rod_vs_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_saver "Выгрузка Классификатора 'Перечень родов войск, специальных войск и служб'"
        
        ./classsaver -ng -s scen_save_cl_vid_vs_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_saver "Выгрузка Классификатора 'Перечень видов Вооруженных Сил'"
        
        ./classsaver -ng -s scen_save_cl_specialization_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования специализации'.xml" & progress_saver "Выгрузка Классификатора 'Наименования специализации'"
        
        ./classsaver -ng -s scen_save_cl_state_names_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_saver "Выгрузка Классификатора 'Наименования штатов и структурных подразделений'"
    fi
fi




# ТЕСТОВЫЕ УЧЕБНЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 4)); then

    array_of_save_scen_files=( "scen_save_cl_garnizon_uch.xml" "scen_save_cl_gs_test_uch.xml"  "scen_save_cl_mob_events_test_uch.xml" "scen_save_cl_okrug_flot_test_uch.xml"  "scen_save_cl_osoben_test_uch.xml"  "scen_save_cl_pd_test_uch.xml" "scen_save_cl_posts_gp_test_uch.xml" "scen_save_cl_posts_off_test_uch.xml" "scen_save_cl_posts_sold_test_uch.xml" "scen_save_cl_ppv_gd_test_uch.xml" "scen_save_cl_prednaz_test_uch.xml"  "scen_save_cl_rod_vs_test_uch.xml" "scen_save_cl_ship_types_test_uch.xml" "scen_save_cl_specialization_test_uch.xml" "scen_save_cl_state_names_test_uch.xml" "scen_save_cl_vid_vs_test_uch.xml" "scen_save_cl_vvt_types_test_uch.xml")
    
    
    
    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_save_scen_files[@]}; do

        # ищем тег с db_name в сценарии
        host_value=$(grep "<$host_name>.*<.$host_name>" $xml_file | sed -e "s/^.*<$host_name/<$host_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_user в сценарии
        user_value=$(grep "<$user_name>.*<.$user_name>" $xml_file | sed -e "s/^.*<$user_name/<$user_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_pass в сценарии
        password_value=$(grep "<$password>.*<.$password>" $xml_file | sed -e "s/^.*<$password/<$password/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_port в сценарии
        port_value=$(grep "<$port>.*<.$port>" $xml_file | sed -e "s/^.*<$port/<$port/" | cut -f2 -d">"| cut -f1 -d"<")
        
        # переписываем значение внутри тега db_host в сценарии
        sed -e "s/<$host_name>$host_value<\/$host_name>/<$host_name>$host_address_name<\/$host_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_user в сценарии
        sed -e "s/<$user_name>$user_value<\/$user_name>/<$user_name>$base_user_name<\/$user_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_pass в сценарии
        sed -e "s/<$password>$password_value<\/$password>/<$password>$password_base<\/$password>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_port в сценарии
        sed -e "s/<$port>$port_value<\/$port>/<$port>$host_port<\/$port>/g" $xml_file > $temporary
        mv $temporary $xml_file
    done
        
        
    zenity --question --width=300 --height=120 \
        --text="Начать выгрузку классификаторов в формате ИМОД из базы данных sodo_test_uch?" \
        --ok-label="Да" \
        --cancel-label="Нет"

    download_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $download_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_saver () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс выгрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }
    
    
        ./classsaver -ng -s scen_save_cl_vvt_types_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_saver "Выгрузка Классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'"
        
        ./classsaver -ng -s scen_save_cl_ship_types_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Типы кораблей'.xml" & progress_saver "Выгрузка Классификатора 'Типы кораблей'"
        
        ./classsaver -ng -s scen_save_cl_garnizon_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень гарнизонов'.xml" & progress_saver "Выгрузка Классификатора 'Перечень гарнизонов'"
        
        ./classsaver -ng -s scen_save_cl_gs_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименований главных слов'.xml" & progress_saver "Выгрузка Классификатора 'Наименований главных слов'"
        
        ./classsaver -ng -s scen_save_cl_mob_events_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_saver "Выгрузка Классификатора 'Мобилизационные мероприятия'"
        
        ./classsaver -ng -s scen_save_cl_okrug_flot_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_saver "Выгрузка Классификатора 'Перечень военных округов, флотов, флотилий'"
        
        ./classsaver -ng -s scen_save_cl_osoben_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования особенностей'.xml" & progress_saver "Выгрузка Классификатора 'Наименования особенностей'"
        
        ./classsaver -ng -s scen_save_cl_pd_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Пункты дислокации'.xml" & progress_saver "Выгрузка Классификатора 'Пункты дислокации'"
        
        ./classsaver -ng -s scen_save_cl_posts_gp_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей гражданского персонала'"
        
        ./classsaver -ng -s scen_save_cl_posts_off_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей офицеров'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей офицеров'"
        
        ./classsaver -ng -s scen_save_cl_posts_sold_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Штатных должностей солдат'.xml" & progress_saver "Выгрузка Классификатора 'Штатных должностей солдат'"
        
        ./classsaver -ng -s scen_save_cl_ppv_gd_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_saver "Выгрузка Классификатора 'Пунктов погрузки-выгрузки'"
        
        ./classsaver -ng -s scen_save_cl_prednaz_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_saver "Выгрузка Классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'"
        
        ./classsaver -ng -s scen_save_cl_rod_vs_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_saver "Выгрузка Классификатора 'Перечень родов войск, специальных войск и служб'"
        
        ./classsaver -ng -s scen_save_cl_vid_vs_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_saver "Выгрузка Классификатора 'Перечень видов Вооруженных Сил'"
        
        ./classsaver -ng -s scen_save_cl_specialization_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования специализации'.xml" & progress_saver "Выгрузка Классификатора 'Наименования специализации'"
        
        ./classsaver -ng -s scen_save_cl_state_names_test_uch.xml -d ../'Классификаторы в формате ИМОД выгруженные из БД СОДО'/'public'/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_saver "Выгрузка Классификатора 'Наименования штатов и структурных подразделений'"
    fi
fi





#-------------------------------------------------------------------------------------------------------------------------------------------------------------



zenity --width=300 --height=120 --info \
       --text="Классификаторы в формате ИМОД успешно выгружены и сохранены в директорию 'Классификаторы в формате ИМОД выгруженные из БД СОДО'."
