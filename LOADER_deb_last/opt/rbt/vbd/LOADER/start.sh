#!/bin/bash


zenity --width=300 --height=120 --info \
        --ok-label="Далее"\
        --text="Сохраните классификаторы, предназначенные для преобразования, в папку 'opt/rbt/vbd/LOADER/Преобразование в формат ИМОД/public(enemy)/Загрузка исходных файлов в промежуточную базу' и нажмите Далее"




working_mode=$(zenity --width=570 --height=220 --list \
                    --title="Режим работы программы Загрузка классификаторов и ТПК" \
                    --column="" --column="" \
                      1 "Преобразование классификаторов в формат ИМОД" \
                      2 "Загрузка классификаторов в БД СОДО" \
                      3 "Загрузка ТПК в БД СОДО" \
                      4 "Выгрузка классификаторов в формате ИМОД из БД СОДО"
                )

if (($working_mode == 1)); then   

    side_choice=$(zenity --width=570 --height=220 --list \
                        --title="Режим преобразования" \
                        --column="" --column="" \
                        1 "Преобразование классификаторов по своим ВФ" \
                        2 "Преобразование классификаторов по ВФ ИГ" \
                    )                    
    
    if (($side_choice == 1)); then
        cd 'Преобразование в формат ИМОД'/public; sh transformation_to_imod_public.sh
        
    elif (($side_choice == 2)); then
        cd 'Преобразование в формат ИМОД'/enemy; sh transformation_to_imod_enemy.sh
                
    fi            
fi                
                
                
                
                
                
                
if (($working_mode == 2)); then

    side_choice=$(zenity --width=570 --height=220 --list \
                        --title="Режим загрузки" \
                        --column="" --column="" \
                        1 "Загрузка классификаторов по своим ВФ" \
                        2 "Загрузка классификаторов по ВФ ИГ" \
                    )

    # Красные
    if (($side_choice == 1)); then
        
        loader_choice=$(zenity --width=600 --height=220 --list \
                        --title="Режим загрузки" \
                        --column="" --column="" \
                        1 "Загрузка классификаторов в формате ИМОД в БД УПИ через КП ИЛО" \
                        2 "Загрузка классификаторов в формате ИМОД в БД СОДО" \
                        )

        if (($loader_choice == 1)); then
            cd "Загрузка в БД"/public; sh source_loader_with_kp_ilo.sh
            
        elif (($loader_choice == 2)); then
            cd "Загрузка в БД"/public; sh imod_loader.sh
            
        fi
    fi   

        
        
        
        
    # Синие    
    if (($side_choice == 2)); then
        
        loader_choice=$(zenity --width=600 --height=220 --list \
                        --title="Режим загрузки" \
                        --column="" --column="" \
                        1 "Загрузка классификаторов в формате ИМОД в БД УПИ через КП ИЛО" \
                        2 "Загрузка классификаторов в формате ИМОД в БД СОДО" \
                        )

        if (($loader_choice == 1)); then
            cd "Загрузка в БД"/enemy; sh source_loader_with_kp_ilo.sh
            
        elif (($loader_choice == 2)); then
            cd "Загрузка в БД"/enemy; sh imod_loader.sh
            
        fi
    fi 
fi




if (($working_mode == 3)); then
    side_choice=$(zenity --width=570 --height=220 --list \
                        --title="Режим загрузки ТПК" \
                        --column="" --column="" \
                        1 "Загрузка ТПК по своим ВФ" \
                        2 "Загрузка ТПК по ВФ ИГ" \
                    )
    if (($side_choice == 1)); then
        cd "Загрузка в БД"/"Загрузка ТПК"/public; sh tpk_loader_public.sh
    elif (($side_choice == 2)); then
        cd "Загрузка в БД"/"Загрузка ТПК"/enemy; sh tpk_loader_enemy.sh
    fi
fi



if (($working_mode == 4)); then
    side_choice=$(zenity --width=570 --height=220 --list \
                        --title="Выгрузка классификаторов" \
                        --column="" --column="" \
                        1 "Выгрузка классификаторов по своим ВФ" \
                        2 "Выгрузка классификаторов по ВФ ИГ" \
                    )
    if (($side_choice == 1)); then
        cd "Выгрузка из БД"/public; sh public_imod_saver.sh
    elif (($side_choice == 2)); then
        cd "Выгрузка из БД"/enemy; sh enemy_imod_saver.sh
    fi
fi






