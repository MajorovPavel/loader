#!/bin/bash




regime_mode=$(zenity --width=570 --height=220 --list \
                    --title="Режим работы программы" \
                    --column="" --column="" \
                      1 "Тестовые классификаторы" \
                      2 "Боевые классификаторы" \
                      3 "Учебные классификаторы" \
                )
regime_mode_choice=$?                
if [[ $regime_mode_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi


# Функция по отображению шкалы прогресса преобразования при создании промежуточной бд и загрузки в нее классификаторов

prepare_preobr () {
(
echo "10"; sleep 0.5
echo "50"; sleep 0.5
echo "80"; sleep 0.5
echo "100"; sleep 0.5
) | 
zenity --width=400 --height=150 --progress \
                    --text="Подготовка классификаторов для преобразования." \
                    --title="Процесс преобразования классификаторов." \
                    --percentage=0 \
                    --pulsate \
                    --auto-close
}


# Функция по отображению шкалы прогресса преобразования при выгрузке из промежуточной БД и создании файлов ИМОД

progress_preobr () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс преобразования классификаторов." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }



#-------------------------------------------------------------------------------------------------------------------------------------------------------------
# Ввод данных 
# 
# username=$(zenity --width=200 --height=120 --entry --title="" --text="Введите имя пользователя")
# username_choice=$?
# if [[ $username_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
#     exit
# fi
#-------------------------------------------------------------------------------------------------------------------------------------------------------------




#------------------------------------------------------------------------------------------------------------------------------------------------------------- 
# Создание промежуточной базы данных для преобразования исходных классификаторов в формат ИМОД

zenity --width=300 --height=120 --info \
       --text="Классификаторы будут преобразованы в формат ИМОД. Нажмите кнопку ОК и дождитесь сообщения об успешном преобразовании."
       

prepare_preobr
psql -U postgres -c "CREATE DATABASE sodo_test_int";
psql -U postgres -d sodo_test_int -c "CREATE SCHEMA enemy"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_bg_level
(
bg_id integer NOT NULL,
full_name character varying(30),
short_name character varying(15),
UNIQUE(bg_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_bs_level
(
bs_id integer NOT NULL,
full_name character varying(30),
short_name character varying(15),
UNIQUE(bs_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_charact_actions
(
char_act_id integer NOT NULL,
full_name text,
UNIQUE(char_act_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_character_ootvd
(
char_ootvd_id integer NOT NULL,
full_name text,
UNIQUE(char_ootvd_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_countries
(
co_id integer NOT NULL,
full_name text,
short_name text,
UNIQUE(co_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_include_types_ros
(
kod_prizn_org_pod integer NOT NULL,
prizn_org_pod character varying(1) NOT NULL,
full_name text,
short_name text,
UNIQUE(kod_prizn_org_pod)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_importance
(
imptnc_id integer NOT NULL,
full_name character varying(50),
UNIQUE(imptnc_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_nuclear_types
(
nucl_type_id integer NOT NULL,
full_name character varying(50),
short_name character varying(10),
UNIQUE(nucl_type_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_ou_types
(
ou_type_id integer NOT NULL,
full_name text,
UNIQUE(ou_type_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_vf_types
(
type_id integer NOT NULL,
full_name text,
local_name text,
UNIQUE(type_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_vid_vs
(
vid_id integer NOT NULL,
full_name character varying(100),
short_name character varying(50),
UNIQUE(vid_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_ootvd_types
(
ootvd_type_id integer NOT NULL,
full_name character varying(255),
UNIQUE(ootvd_type_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_vvt_types
(
vvt_type_id character varying(12) NOT NULL,
full_name text,
UNIQUE(vvt_type_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_vvt_samples
(
vvt_sample_id integer NOT NULL,
full_name text,
UNIQUE(vvt_sample_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_vgd
(
vgd_id character varying(14) NOT NULL,
co_id integer,
unit_id smallint,
full_name character varying(80),
short_name character varying(30),
UNIQUE(vgd_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_vgd_units
(
unit_id smallint NOT NULL,
full_name character varying(80),
short_name character varying(30),
UNIQUE(unit_id)
);"



psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_oceans
(
ocean_id smallint NOT NULL,
full_name character varying(80),
short_name character varying(30),
UNIQUE(ocean_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_alliances
(
allnc_id integer NOT NULL,
full_name character varying(100),
short_name character varying(100),
UNIQUE(allnc_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_aqua
(
ocean_id smallint NOT NULL,
aqua_id integer NOT NULL,
full_name character varying(50),
short_name character varying(25),
UNIQUE(ocean_id, aqua_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_levels_vf
(
level_id integer NOT NULL,
full_name character varying(50),
short_name character varying(25),
UNIQUE(level_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_rod
(
rod_id integer NOT NULL,
full_name character varying(80),
short_name character varying(30),
UNIQUE(rod_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_eop
(
eop_id integer NOT NULL,
full_name character varying(50),
short_name character varying(25),
UNIQUE(eop_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_levels_gr
(
level_id integer NOT NULL,
full_name character varying(50),
short_name character varying(25),
UNIQUE(level_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_sostav_gr
(
sostav_id integer NOT NULL,
full_name character varying(50),
short_name character varying(25),
UNIQUE(sostav_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_scales_gr
(
scale_id integer NOT NULL,
full_name character varying(50),
short_name character varying(25),
UNIQUE(scale_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.cl_predn_gr
(
predn_id integer NOT NULL,
full_name character varying(50),
short_name character varying(25),
UNIQUE(predn_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE enemy.sl_prizn_razv
(
prizn_razv_id integer NOT NULL,
full_name character varying(50),
UNIQUE(prizn_razv_id)
);"
#   ЗАГРУЗКА КЛАССИФИКАТОРОВ-ИСХОДНИКОВ

prepare_preobr

cd 'Загрузка исходных файлов в промежуточную базу'




./classloader -ng -s scen_load_cl_bg_level.xml -d section.xml & prepare_preobr

./classloader -ng -s scen_load_cl_bs_level.xml -d section.xml

./classloader -ng -s scen_load_cl_charact_actions.xml -d section.xml

./classloader -ng -s scen_load_cl_character_ootvd.xml -d section.xml & prepare_preobr

./classloader -ng -s scen_load_cl_countries.xml -d section.xml

./classloader -ng -s scen_load_cl_include_types_ros.xml -d section.xml & prepare_preobr

./classloader -ng -s scen_load_cl_importance.xml -d section.xml

./classloader -ng -s scen_load_cl_nuclear_types.xml -d section.xml & prepare_preobr

./classloader -ng -s scen_load_cl_ou_types.xml -d section.xml

./classloader -ng -s scen_load_cl_vf_types.xml -d section.xml

./classloader -ng -s scen_load_cl_vid_vs.xml -d section.xml & prepare_preobr

./classloader -ng -s scen_load_cl_ootvd_types.xml -d section.xml

./classloader -ng -s scen_load_cl_vvt_types.xml -d section.xml

./classloader -ng -s scen_load_cl_vvt_samples.xml -d section.xml & prepare_preobr

./classloader -ng -s scen_load_cl_vgd_units.xml -d section.xml

./classloader -ng -s scen_load_cl_oceans.xml -d section.xml & prepare_preobr

./classloader -ng -s scen_load_cl_aqua.xml -d section.xml

./classloader -ng -s scen_load_cl_alliances.xml -d section.xml & prepare_preobr

./classloader -ng -s scen_load_cl_levels_vf.xml -d section.xml

./classloader -ng -s scen_load_cl_rod.xml -d section.xml

./classloader -ng -s scen_load_cl_eop.xml -d section.xml & prepare_preobr

./classloader -ng -s scen_load_cl_levels_gr.xml -d section.xml

./classloader -ng -s scen_load_cl_sostav_gr.xml -d section.xml

./classloader -ng -s scen_load_cl_scales_gr.xml -d section.xml & prepare_preobr

./classloader -ng -s scen_load_cl_predn_gr.xml -d section.xml

./classloader -ng -s scen_load_sl_prizn_razv.xml -d section.xml

./classloader -ng -s scen_load_cl_vgd.xml -d section.xml & prepare_preobr


cd ../

#ВЫГРУЗКА КЛАССИФИКАТОРОВ(IMOD)

cd 'Выгрузка в формате имод из промежуточной базы'



# ТЕСТОВЫЕ КЛАССИФИКАТОРЫ
mkdir /'tmp'/'Классификаторы в формате ИМОД'/
if (($regime_mode == 1)); then 

    ./classsaver -ng -s scen_save_cl_bg_level_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_preobr "Преобразование классификатора 'Степени боевой готовности ВФ ИГ'"

    ./classsaver -ng -s scen_save_cl_bs_level_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Степени боевой способности ВФ ИГ'.xml" & progress_preobr "Преобразование классификатора 'Степени боевой способности ВФ ИГ'"

    ./classsaver -ng -s scen_save_cl_charact_actions_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Характер действий'.xml" & progress_preobr "Преобразование классификатора 'Характер действий'"

    ./classsaver -ng -s scen_save_cl_character_ootvd_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Характеристики объектов учета оперативного оборудования театра военных действий'.xml" & progress_preobr "Преобразование классификатора 'Характеристики объектов учета оперативного оборудования театра военных действий'"

    ./classsaver -ng -s scen_save_cl_countries_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Страны мира ИГ'.xml" & progress_preobr "Преобразование классификатора 'Страны мира ИГ'"

    ./classsaver -ng -s scen_save_cl_include_types_ros_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Виды оргподчиненности формирований ВС'.xml"  & progress_preobr "Преобразование классификатора 'Виды оргподчиненности формирований ВС'"

    ./classsaver -ng -s scen_save_cl_importance_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Признаки важности объектов учета'.xml" & progress_preobr "Преобразование классификатора 'Признаки важности объектов учета'"

    ./classsaver -ng -s scen_save_cl_nuclear_types_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Признаки носителя ядерного оружия ИГ'.xml" & progress_preobr "Преобразование классификатора 'Признаки носителя ядерного оружия ИГ'"


    ./classsaver -ng -s scen_save_cl_ou_types_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Виды объектов учета'.xml" & progress_preobr "Преобразование классификатора 'Виды объектов учета'"


    ./classsaver -ng -s scen_save_cl_vf_types_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы формирований ВС ИГ'.xml" & progress_preobr "Преобразование классификатора 'Типы формирований ВС ИГ'"

    ./classsaver -ng -s scen_save_cl_vid_vs_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Виды вооруженных сил'.xml" & progress_preobr "Преобразование классификатора 'Виды вооруженных сил'"

    ./classsaver -ng -s scen_save_cl_ootvd_types_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы объектов оперативного оборудования театра военных действий'.xml" & progress_preobr "Преобразование классификатора 'Типы объектов оперативного оборудования театра военных действий'"


    ./classsaver -ng -s scen_save_cl_vvt_types_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы вооружения и военной техники'.xml" & progress_preobr "Преобразование классификатора 'Типы вооружения и военной техники'"


    ./classsaver -ng -s scen_save_cl_vvt_samples_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Конкретные образцы вооружения и военной техники'.xml" & progress_preobr "Преобразование классификатора 'Конкретные образцы вооружения и военной техники'"


    ./classsaver -ng -s scen_save_cl_vgd_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Военно-географическое деление'.xml" & progress_preobr "Преобразование классификатора 'Военно-географическое деление'"


    ./classsaver -ng -s scen_save_cl_vgd_units_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Единиц военно-географического деления'.xml" & progress_preobr "Преобразование классификатора 'Единиц военно-географического деления'"


    ./classsaver -ng -s scen_save_cl_oceans_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Названия океанов'.xml" & progress_preobr "Преобразование классификатора 'Названия океанов'"


    ./classsaver -ng -s scen_save_cl_aqua_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Акватории'.xml" & progress_preobr "Преобразование классификатора 'Акватории'"


    ./classsaver -ng -s scen_save_cl_alliances_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Названия блоков'.xml" & progress_preobr "Преобразование классификатора 'Названия блоков'"


    ./classsaver -ng -s scen_save_cl_levels_vf_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Уровни воинских формирований'.xml" & progress_preobr "Преобразование классификатора 'Уровни воинских формирований'"


    ./classsaver -ng -s scen_save_cl_rod_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Рода войск'.xml" & progress_preobr "Преобразование классификатора 'Рода войск'"


    ./classsaver -ng -s scen_save_cl_eop_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Элементы оперативного построения (боевого порядка) объектов противника'.xml" & progress_preobr "Преобразование классификатора 'Элементы оперативного построения (боевого порядка) объектов противника'"


    ./classsaver -ng -s scen_save_cl_levels_gr_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Уровни группировок'.xml" & progress_preobr "Преобразование классификатора 'Уровни группировок'"


    ./classsaver -ng -s scen_save_cl_sostav_gr_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Составы группировок'.xml" & progress_preobr "Преобразование классификатора 'Составы группировок'"


    ./classsaver -ng -s scen_save_cl_scales_gr_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Масштабы группировок'.xml" & progress_preobr "Преобразование классификатора 'Масштабы группировок'"


    ./classsaver -ng -s scen_save_cl_predn_gr_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Предназначения группировок'.xml" & progress_preobr "Преобразование классификатора 'Предназначения группировок'"


    ./classsaver -ng -s scen_save_sl_prizn_razv_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Признаки объекта разведки и поражения'.xml" & progress_preobr "Преобразование классификатора 'Признаки объекта разведки и поражения'"
fi




# БОЕВЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 2)); then 

    ./classsaver -ng -s scen_save_cl_bg_level_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_preobr "Преобразование классификатора 'Степени боевой готовности ВФ ИГ'"

    ./classsaver -ng -s scen_save_cl_bs_level_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Степени боевой способности ВФ ИГ'.xml" & progress_preobr "Преобразование классификатора 'Степени боевой способности ВФ ИГ'"

    ./classsaver -ng -s scen_save_cl_charact_actions_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Характер действий'.xml" & progress_preobr "Преобразование классификатора 'Характер действий'"

    ./classsaver -ng -s scen_save_cl_character_ootvd_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Характеристики объектов учета оперативного оборудования театра военных действий'.xml" & progress_preobr "Преобразование классификатора 'Характеристики объектов учета оперативного оборудования театра военных действий'"

    ./classsaver -ng -s scen_save_cl_countries_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Страны мира ИГ'.xml" & progress_preobr "Преобразование классификатора 'Страны мира ИГ'"

    ./classsaver -ng -s scen_save_cl_include_types_ros_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Виды оргподчиненности формирований ВС'.xml"  & progress_preobr "Преобразование классификатора 'Виды оргподчиненности формирований ВС'"

    ./classsaver -ng -s scen_save_cl_importance_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Признаки важности объектов учета'.xml" & progress_preobr "Преобразование классификатора 'Признаки важности объектов учета'"

    ./classsaver -ng -s scen_save_cl_nuclear_types_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Признаки носителя ядерного оружия ИГ'.xml" & progress_preobr "Преобразование классификатора 'Признаки носителя ядерного оружия ИГ'"


    ./classsaver -ng -s scen_save_cl_ou_types_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Виды объектов учета'.xml" & progress_preobr "Преобразование классификатора 'Виды объектов учета'"


    ./classsaver -ng -s scen_save_cl_vf_types_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы формирований ВС ИГ'.xml" & progress_preobr "Преобразование классификатора 'Типы формирований ВС ИГ'"

    ./classsaver -ng -s scen_save_cl_vid_vs_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Виды вооруженных сил'.xml" & progress_preobr "Преобразование классификатора 'Виды вооруженных сил'"

    ./classsaver -ng -s scen_save_cl_ootvd_types_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы объектов оперативного оборудования театра военных действий'.xml" & progress_preobr "Преобразование классификатора 'Типы объектов оперативного оборудования театра военных действий'"


    ./classsaver -ng -s scen_save_cl_vvt_types_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы вооружения и военной техники'.xml" & progress_preobr "Преобразование классификатора 'Типы вооружения и военной техники'"


    ./classsaver -ng -s scen_save_cl_vvt_samples_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Конкретные образцы вооружения и военной техники'.xml" & progress_preobr "Преобразование классификатора 'Конкретные образцы вооружения и военной техники'"


    ./classsaver -ng -s scen_save_cl_vgd_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Военно-географическое деление'.xml" & progress_preobr "Преобразование классификатора 'Военно-географическое деление'"


    ./classsaver -ng -s scen_save_cl_vgd_units_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Единиц военно-географического деления'.xml" & progress_preobr "Преобразование классификатора 'Единиц военно-географического деления'"


    ./classsaver -ng -s scen_save_cl_oceans_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Названия океанов'.xml" & progress_preobr "Преобразование классификатора 'Названия океанов'"


    ./classsaver -ng -s scen_save_cl_aqua_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Акватории'.xml" & progress_preobr "Преобразование классификатора 'Акватории'"


    ./classsaver -ng -s scen_save_cl_alliances_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Названия блоков'.xml" & progress_preobr "Преобразование классификатора 'Названия блоков'"


    ./classsaver -ng -s scen_save_cl_levels_vf_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Уровни воинских формирований'.xml" & progress_preobr "Преобразование классификатора 'Уровни воинских формирований'"


    ./classsaver -ng -s scen_save_cl_rod_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Рода войск'.xml" & progress_preobr "Преобразование классификатора 'Рода войск'"


    ./classsaver -ng -s scen_save_cl_eop_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Элементы оперативного построения (боевого порядка) объектов противника'.xml" & progress_preobr "Преобразование классификатора 'Элементы оперативного построения (боевого порядка) объектов противника'"


    ./classsaver -ng -s scen_save_cl_levels_gr_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Уровни группировок'.xml" & progress_preobr "Преобразование классификатора 'Уровни группировок'"


    ./classsaver -ng -s scen_save_cl_sostav_gr_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Составы группировок'.xml" & progress_preobr "Преобразование классификатора 'Составы группировок'"


    ./classsaver -ng -s scen_save_cl_scales_gr_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Масштабы группировок'.xml" & progress_preobr "Преобразование классификатора 'Масштабы группировок'"


    ./classsaver -ng -s scen_save_cl_predn_gr_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Предназначения группировок'.xml" & progress_preobr "Преобразование классификатора 'Предназначения группировок'"


    ./classsaver -ng -s scen_save_sl_prizn_razv_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Признаки объекта разведки и поражения'.xml" & progress_preobr "Преобразование классификатора 'Признаки объекта разведки и поражения'"
fi




# УЧЕБНЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 3)); then 

    ./classsaver -ng -s scen_save_cl_bg_level_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_preobr "Преобразование классификатора 'Степени боевой готовности ВФ ИГ'"

    ./classsaver -ng -s scen_save_cl_bs_level_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Степени боевой способности ВФ ИГ'.xml" & progress_preobr "Преобразование классификатора 'Степени боевой способности ВФ ИГ'"

    ./classsaver -ng -s scen_save_cl_charact_actions_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Характер действий'.xml" & progress_preobr "Преобразование классификатора 'Характер действий'"

    ./classsaver -ng -s scen_save_cl_character_ootvd_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Характеристики объектов учета оперативного оборудования театра военных действий'.xml" & progress_preobr "Преобразование классификатора 'Характеристики объектов учета оперативного оборудования театра военных действий'"

    ./classsaver -ng -s scen_save_cl_countries_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Страны мира ИГ'.xml" & progress_preobr "Преобразование классификатора 'Страны мира ИГ'"

    ./classsaver -ng -s scen_save_cl_include_types_ros_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Виды оргподчиненности формирований ВС'.xml"  & progress_preobr "Преобразование классификатора 'Виды оргподчиненности формирований ВС'"

    ./classsaver -ng -s scen_save_cl_importance_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Признаки важности объектов учета'.xml" & progress_preobr "Преобразование классификатора 'Признаки важности объектов учета'"

    ./classsaver -ng -s scen_save_cl_nuclear_types_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Признаки носителя ядерного оружия ИГ'.xml" & progress_preobr "Преобразование классификатора 'Признаки носителя ядерного оружия ИГ'"


    ./classsaver -ng -s scen_save_cl_ou_types_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Виды объектов учета'.xml" & progress_preobr "Преобразование классификатора 'Виды объектов учета'"


    ./classsaver -ng -s scen_save_cl_vf_types_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы формирований ВС ИГ'.xml" & progress_preobr "Преобразование классификатора 'Типы формирований ВС ИГ'"

    ./classsaver -ng -s scen_save_cl_vid_vs_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Виды вооруженных сил'.xml" & progress_preobr "Преобразование классификатора 'Виды вооруженных сил'"

    ./classsaver -ng -s scen_save_cl_ootvd_types_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы объектов оперативного оборудования театра военных действий'.xml" & progress_preobr "Преобразование классификатора 'Типы объектов оперативного оборудования театра военных действий'"


    ./classsaver -ng -s scen_save_cl_vvt_types_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы вооружения и военной техники'.xml" & progress_preobr "Преобразование классификатора 'Типы вооружения и военной техники'"


    ./classsaver -ng -s scen_save_cl_vvt_samples_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Конкретные образцы вооружения и военной техники'.xml" & progress_preobr "Преобразование классификатора 'Конкретные образцы вооружения и военной техники'"


    ./classsaver -ng -s scen_save_cl_vgd_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Военно-географическое деление'.xml" & progress_preobr "Преобразование классификатора 'Военно-географическое деление'"


    ./classsaver -ng -s scen_save_cl_vgd_units_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Единиц военно-географического деления'.xml" & progress_preobr "Преобразование классификатора 'Единиц военно-географического деления'"


    ./classsaver -ng -s scen_save_cl_oceans_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Названия океанов'.xml" & progress_preobr "Преобразование классификатора 'Названия океанов'"


    ./classsaver -ng -s scen_save_cl_aqua_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Акватории'.xml" & progress_preobr "Преобразование классификатора 'Акватории'"


    ./classsaver -ng -s scen_save_cl_alliances_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Названия блоков'.xml" & progress_preobr "Преобразование классификатора 'Названия блоков'"


    ./classsaver -ng -s scen_save_cl_levels_vf_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Уровни воинских формирований'.xml" & progress_preobr "Преобразование классификатора 'Уровни воинских формирований'"


    ./classsaver -ng -s scen_save_cl_rod_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Рода войск'.xml" & progress_preobr "Преобразование классификатора 'Рода войск'"


    ./classsaver -ng -s scen_save_cl_eop_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Элементы оперативного построения (боевого порядка) объектов противника'.xml" & progress_preobr "Преобразование классификатора 'Элементы оперативного построения (боевого порядка) объектов противника'"


    ./classsaver -ng -s scen_save_cl_levels_gr_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Уровни группировок'.xml" & progress_preobr "Преобразование классификатора 'Уровни группировок'"


    ./classsaver -ng -s scen_save_cl_sostav_gr_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Составы группировок'.xml" & progress_preobr "Преобразование классификатора 'Составы группировок'"


    ./classsaver -ng -s scen_save_cl_scales_gr_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Масштабы группировок'.xml" & progress_preobr "Преобразование классификатора 'Масштабы группировок'"


    ./classsaver -ng -s scen_save_cl_predn_gr_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Предназначения группировок'.xml" & progress_preobr "Преобразование классификатора 'Предназначения группировок'"


    ./classsaver -ng -s scen_save_sl_prizn_razv_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Признаки объекта разведки и поражения'.xml" & progress_preobr "Преобразование классификатора 'Признаки объекта разведки и поражения'"
fi


cd ../



# здесь удалить промежуточную базу
# Перед удалением промежуточной БД проверяем, что нет подключенных к ней пользователей
psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'sodo_test_int' AND pid <> pg_backend_pid()"

dropdb sodo_test_int -U postgres


#-------------------------------------------------------------------------------------------------------------------------------------------------------------


zenity --question --width=300 --height=120 \
--text="Классификаторы успешно преобразованы. Для перехода в главное меню нажмите Далее, для выхода из программы нажмите Выход." \
--ok-label="Далее" \
--cancel-label="Выход"


continue_choice=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

if [[ $continue_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit

else
    cd /opt/rbt/vbd/LOADER; sh start.sh
fi
