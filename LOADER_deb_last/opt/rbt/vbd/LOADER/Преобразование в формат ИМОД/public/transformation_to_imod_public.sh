#!/bin/bash



regime_mode=$(zenity --width=570 --height=220 --list \
                    --title="Режим работы программы" \
                    --column="" --column="" \
                      1 "Тестовые классификаторы" \
                      2 "Боевые классификаторы" \
                      3 "Учебные классификаторы" \
                )
regime_mode_choice=$?                
if [[ $regime_mode_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi


# Функция по отображению шкалы прогресса преобразования

prepare_preobr () {
(
echo "10"; sleep 0.5
echo "50"; sleep 0.5
echo "80"; sleep 0.5
echo "100"; sleep 0.5
) | 
zenity --width=400 --height=150 --progress \
                    --text="Подготовка классификаторов для преобразования." \
                    --title="Процесс преобразования классификаторов." \
                    --percentage=0 \
                    --pulsate \
                    --auto-close
}



progress_preobr () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс преобразования классификаторов." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }


#-------------------------------------------------------------------------------------------------------------------------------------------------------------

# Ввод данных 



# username=$(zenity --width=200 --height=120 --entry --title="" --text="Введите имя пользователя")
# username_choice=$?
# if [[ $username_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
#     exit
# fi










#-------------------------------------------------------------------------------------------------------------------------------------------------------------  





#------------------------------------------------------------------------------------------------------------------------------------------------------------- 
# Создание промежуточной базы данных для преобразования исходных классификаторов в формат ИМОД

zenity --width=300 --height=120 --info \
       --text="Классификаторы будут преобразованы в формат ИМОД. Нажмите кнопку ОК и дождитесь сообщения об успешном преобразовании."
       
prepare_preobr       
psql -U postgres -c "CREATE DATABASE sodo_test_int";

psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_vvt_types
(
vvt_type_id character(9) NOT NULL,
full_name character varying(255),
short_name character varying(125),
UNIQUE(vvt_type_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_bg_level
(
bg_id int2 NOT NULL,
full_name character varying(20),
short_name character varying(10),
UNIQUE(bg_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_ship_types
(
sh_t_id character varying(5) NOT NULL,
full_name character varying(255),
project_name character varying(50),
UNIQUE(sh_t_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_mob_events
(
me_id character(3) NOT NULL,
full_name character varying(255),
UNIQUE(me_id)
);"


psql -U postgres -d sodo_test_int -c  "CREATE TABLE public.cl_gs
(
gs_id character varying(3) NOT NULL,
full_name character varying(128),
UNIQUE(gs_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_osoben
(
osob_id character varying(3),
full_name character varying(255),
UNIQUE(osob_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_state_names
(
st_name_id character(6) NOT NULL,
gs_id character varying(3),
osob_id character varying(3),
spec_id character varying(4),
full_name character varying(255),
short_name character varying(125),
UNIQUE(st_name_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_vid_vs
(
vid_id character varying(2) NOT NULL,
full_name character varying(255),
short_name character varying(10),
UNIQUE(vid_id)
);"



psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_okrug_flot
(
of_id character varying(20) NOT NULL,
full_name character varying(255),
short_name character varying(10),
UNIQUE(of_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_garnizon
(
gar_id character(5) NOT NULL,
full_name character varying(255),
UNIQUE(gar_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_prednaz
(
predn_id character(3) NOT NULL,
full_name character varying(255),
UNIQUE(predn_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_rod_vs
(
rod_id character(3) NOT NULL,
full_name character varying(255),
short_name character varying(10),
UNIQUE(rod_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_ppv_gd
(
ppv_id character(6) NOT NULL,
full_name character varying(255),
UNIQUE(ppv_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_specialization
(
spec_id character varying(4),
full_name character varying(255),
UNIQUE(spec_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_posts_gp
(
post_id character(4) NOT NULL,
full_name character varying(256),
UNIQUE(post_id)
);"

psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_posts_off
(
post_id integer NOT NULL,
full_name character varying(255),
UNIQUE(post_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_posts_sold
(
post_id integer NOT NULL,
full_name character varying(255),
UNIQUE(post_id)
);"


psql -U postgres -d sodo_test_int -c "CREATE TABLE public.cl_pd
(
pd_id character(10) NOT NULL,
full_name character varying(256),
UNIQUE(pd_id)
);"
#-------------------------------------------------------------------------------------------------------------------------------------------------------------



#-------------------------------------------------------------------------------------------------------------------------------------------------------------
#   Загрузка-выгрузка классификаторов-исходников в промежуточную базу для преобразования в ИМОД


cd 'Загрузка исходных файлов в промежуточную базу'


./classloader -ng -s scen_load_cl_bg_level_r4036.xml -d r4036.pts.xml & prepare_preobr

./classloader -ng -s scen_load_cl_garnizon_r9017.xml -d r9017.pts.xml & prepare_preobr

./classloader -ng -s scen_load_cl_gs_r9065.xml -d r9065.pts.xml & prepare_preobr

./classloader -ng -s scen_load_cl_mob_events_r9007.xml -d r9007.pts.xml

./classloader -ng -s scen_load_cl_okrug_flot_r9004.xml -d r9004.pts.xml & prepare_preobr

./classloader -ng -s scen_load_cl_osoben_r9061.xml -d r9061.pts.xml

./classloader -ng -s scen_load_cl_pd_r2006.xml -d r2006.pts.xml & prepare_preobr

./classloader -ng -s scen_load_cl_posts_gp_r9170.xml -d r9170.pts.xml

./classloader -ng -s scen_load_cl_posts_off_r4057.xml -d r4057.pts.xml & prepare_preobr

./classloader -ng -s scen_load_cl_posts_sold_r4061.xml -d r4061.pts.xml & prepare_preobr

./classloader -ng -s scen_load_cl_ppv_gd_r1095.xml -d r1095.pts.xml & prepare_preobr

./classloader -ng -s scen_load_cl_prednaz_r9015.xml -d r9015.pts.xml

./classloader -ng -s scen_load_cl_rod_vs_r9014.xml -d r9014.pts.xml & prepare_preobr

./classloader -ng -s scen_load_ship_types_m0001.xml -d m0001.pts.xml

./classloader -ng -s scen_load_cl_specialization_r9062.xml -d r9062.pts.xml

./classloader -ng -s scen_load_cl_state_names_r9060.xml -d r9060.pts.xml & prepare_preobr

./classloader -ng -s scen_load_cl_vid_vs_r9013.xml -d r9013.pts.xml & prepare_preobr

./classloader -ng -s scen_load_cl_vvt_types_r9460.xml -d r9460.pts.xml & prepare_preobr
# ./classloader -ng -s scen_load_cl_vzv_r4058.xml -d r4058.pts.xml     cl_vzv не грузим


cd ../

#Выгрузка классификаторов в формате ИМОД

cd 'Выгрузка в формате имод из промежуточной базы'



# ТЕСТОВЫЕ КЛАССИФИКАТОРЫ
    mkdir /'tmp'/'Классификаторы в формате ИМОД'/
if (($regime_mode == 1)); then
    ./classsaver -ng -s scen_save_cl_bg_level_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Боевая готовность'.xml" & progress_preobr "Преобразование классификатора 'Боевая готовность'"
    
    ./classsaver -ng -s scen_save_cl_vvt_types_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_preobr "Преобразование классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'"
    
    ./classsaver -ng -s scen_save_cl_ship_types_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы кораблей'.xml" & progress_preobr "Преобразование классификатора 'Типы кораблей'"
    
    ./classsaver -ng -s scen_save_cl_garnizon_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень гарнизонов'.xml" & progress_preobr "Преобразование классификатора 'Перечень гарнизонов'"
    
    ./classsaver -ng -s scen_save_cl_gs_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименований главных слов'.xml" & progress_preobr "Преобразование классификатора 'Наименований главных слов'"
    
    ./classsaver -ng -s scen_save_cl_mob_events_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_preobr "Преобразование классификатора 'Мобилизационные мероприятия'"
    
    ./classsaver -ng -s scen_save_cl_okrug_flot_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_preobr "Преобразование классификатора 'Перечень военных округов, флотов, флотилий'"
    
    ./classsaver -ng -s scen_save_cl_osoben_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименования особенностей'.xml" & progress_preobr "Преобразование классификатора 'Наименования особенностей'"
    
    ./classsaver -ng -s scen_save_cl_pd_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Пункты дислокации'.xml" & progress_preobr "Преобразование классификатора 'Пункты дислокации'"
    
    ./classsaver -ng -s scen_save_cl_posts_gp_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_preobr "Преобразование классификатора 'Штатных должностей гражданского персонала'"
    
    ./classsaver -ng -s scen_save_cl_posts_off_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Штатных должностей офицеров'.xml" & progress_preobr "Преобразование классификатора 'Штатных должностей офицеров'"
    
    ./classsaver -ng -s scen_save_cl_posts_sold_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Штатных должностей солдат'.xml" & progress_preobr "Преобразование классификатора 'Штатных должностей солдат'"
    
    ./classsaver -ng -s scen_save_cl_ppv_gd_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_preobr "Преобразование классификатора 'Пунктов погрузки-выгрузки'"
    
    ./classsaver -ng -s scen_save_cl_prednaz_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_preobr "Преобразование классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'"
    
    ./classsaver -ng -s scen_save_cl_rod_vs_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_preobr "Преобразование классификатора 'Перечень родов войск, специальных войск и служб'"
    
    ./classsaver -ng -s scen_save_cl_vid_vs_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_preobr "Преобразование классификатора 'Перечень видов Вооруженных Сил'"
    
    ./classsaver -ng -s scen_save_cl_specialization_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименования специализации'.xml" & progress_preobr "Преобразование классификатора 'Наименования специализации'"
    
    ./classsaver -ng -s scen_save_cl_state_names_test.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_preobr "Преобразование классификатора 'Наименования штатов и структурных подразделений'"
fi


# БОЕВЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 2)); then
    ./classsaver -ng -s scen_save_cl_bg_level_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Боевая готовность'.xml" & progress_preobr "Преобразование классификатора 'Боевая готовность'"
    
    ./classsaver -ng -s scen_save_cl_vvt_types_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_preobr "Преобразование классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'"
    
    ./classsaver -ng -s scen_save_cl_ship_types_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы кораблей'.xml" & progress_preobr "Преобразование классификатора 'Типы кораблей'"
    
    ./classsaver -ng -s scen_save_cl_garnizon_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень гарнизонов'.xml" & progress_preobr "Преобразование классификатора 'Перечень гарнизонов'"
    
    ./classsaver -ng -s scen_save_cl_gs_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименований главных слов'.xml" & progress_preobr "Преобразование классификатора 'Наименований главных слов'"
    
    ./classsaver -ng -s scen_save_cl_mob_events_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_preobr "Преобразование классификатора 'Мобилизационные мероприятия'"
    
    ./classsaver -ng -s scen_save_cl_okrug_flot_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_preobr "Преобразование классификатора 'Перечень военных округов, флотов, флотилий'"
    
    ./classsaver -ng -s scen_save_cl_osoben_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименования особенностей'.xml" & progress_preobr "Преобразование классификатора 'Наименования особенностей'"
    
    ./classsaver -ng -s scen_save_cl_pd_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Пункты дислокации'.xml" & progress_preobr "Преобразование классификатора 'Пункты дислокации'"
    
    ./classsaver -ng -s scen_save_cl_posts_gp_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_preobr "Преобразование классификатора 'Штатных должностей гражданского персонала'"
    
    ./classsaver -ng -s scen_save_cl_posts_off_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Штатных должностей офицеров'.xml" & progress_preobr "Преобразование классификатора 'Штатных должностей офицеров'"
    
    ./classsaver -ng -s scen_save_cl_posts_sold_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Штатных должностей солдат'.xml" & progress_preobr "Преобразование классификатора 'Штатных должностей солдат'"
    
    ./classsaver -ng -s scen_save_cl_ppv_gd_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_preobr "Преобразование классификатора 'Пунктов погрузки-выгрузки'"
    
    ./classsaver -ng -s scen_save_cl_prednaz_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_preobr "Преобразование классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'"
    
    ./classsaver -ng -s scen_save_cl_rod_vs_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_preobr "Преобразование классификатора 'Перечень родов войск, специальных войск и служб'"
    
    ./classsaver -ng -s scen_save_cl_vid_vs_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_preobr "Преобразование классификатора 'Перечень видов Вооруженных Сил'"
    
    ./classsaver -ng -s scen_save_cl_specialization_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименования специализации'.xml" & progress_preobr "Преобразование классификатора 'Наименования специализации'"
    
    ./classsaver -ng -s scen_save_cl_state_names_battle.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_preobr "Преобразование классификатора 'Наименования штатов и структурных подразделений'"
fi




# УЧЕБНЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 3)); then
    ./classsaver -ng -s scen_save_cl_bg_level_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Боевая готовность'.xml" & progress_preobr "Преобразование классификатора 'Боевая готовность'"
    
    ./classsaver -ng -s scen_save_cl_vvt_types_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_preobr "Преобразование классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'"
    
    ./classsaver -ng -s scen_save_cl_ship_types_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Типы кораблей'.xml" & progress_preobr "Преобразование классификатора 'Типы кораблей'"
    
    ./classsaver -ng -s scen_save_cl_garnizon_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень гарнизонов'.xml" & progress_preobr "Преобразование классификатора 'Перечень гарнизонов'"
    
    ./classsaver -ng -s scen_save_cl_gs_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименований главных слов'.xml" & progress_preobr "Преобразование классификатора 'Наименований главных слов'"
    
    ./classsaver -ng -s scen_save_cl_mob_events_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_preobr "Преобразование классификатора 'Мобилизационные мероприятия'"
    
    ./classsaver -ng -s scen_save_cl_okrug_flot_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_preobr "Преобразование классификатора 'Перечень военных округов, флотов, флотилий'"
    
    ./classsaver -ng -s scen_save_cl_osoben_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименования особенностей'.xml" & progress_preobr "Преобразование классификатора 'Наименования особенностей'"
    
    ./classsaver -ng -s scen_save_cl_pd_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Пункты дислокации'.xml" & progress_preobr "Преобразование классификатора 'Пункты дислокации'"
    
    ./classsaver -ng -s scen_save_cl_posts_gp_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_preobr "Преобразование классификатора 'Штатных должностей гражданского персонала'"
    
    ./classsaver -ng -s scen_save_cl_posts_off_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Штатных должностей офицеров'.xml" & progress_preobr "Преобразование классификатора 'Штатных должностей офицеров'"
    
    ./classsaver -ng -s scen_save_cl_posts_sold_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Штатных должностей солдат'.xml" & progress_preobr "Преобразование классификатора 'Штатных должностей солдат'"
    
    ./classsaver -ng -s scen_save_cl_ppv_gd_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_preobr "Преобразование классификатора 'Пунктов погрузки-выгрузки'"
    
    ./classsaver -ng -s scen_save_cl_prednaz_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_preobr "Преобразование классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'"
    
    ./classsaver -ng -s scen_save_cl_rod_vs_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_preobr "Преобразование классификатора 'Перечень родов войск, специальных войск и служб'"
    
    ./classsaver -ng -s scen_save_cl_vid_vs_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_preobr "Преобразование классификатора 'Перечень видов Вооруженных Сил'"
    
    ./classsaver -ng -s scen_save_cl_specialization_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименования специализации'.xml" & progress_preobr "Преобразование классификатора 'Наименования специализации'"
    
    ./classsaver -ng -s scen_save_cl_state_names_uch.xml -d /'tmp'/'Классификаторы в формате ИМОД'/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_preobr "Преобразование классификатора 'Наименования штатов и структурных подразделений'"
fi





cd ../


# удаление промежуточной БД
# Перед удалением промежуточной БД проверяем, что нет подключенных к ней пользователей
psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'sodo_test_int' AND pid <> pg_backend_pid()"
dropdb sodo_test_int -U postgres

#-------------------------------------------------------------------------------------------------------------------------------------------------------------

       
       
       
zenity --question --width=300 --height=120 \
--text="Классификаторы успешно преобразованы. Для перехода в главное меню нажмите Далее, для выхода из программы нажмите Выход." \
--ok-label="Далее" \
--cancel-label="Выход"

continue_choice=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

if [[ $continue_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit

else
    cd /opt/rbt/vbd/LOADER; sh start.sh
fi
