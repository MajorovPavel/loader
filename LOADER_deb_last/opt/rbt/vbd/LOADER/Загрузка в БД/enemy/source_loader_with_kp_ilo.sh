#!/bin/bash

#-------------------------------------------------------------------------------------------------------------------------------------------------------------

mkdir /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"

zenity --width=300 --height=120 --info \
        --ok-label='Далее' \
        --text="Классификаторы, для загрузки в БД УПИ, находятся в папке tmp/'Классификаторы в формате ИМОД'. Загрузите имеющиеся классификаторы в БД УПИ средствами КП ИЛО и нажмите кнопку 'Далее'."
        
zenity --width=300 --height=120 --info \
        --ok-label='Далее' \
        --text="Выгрузите классификаторы из БД УПИ средствами КП ИЛО, сохраните полученные классификаторы в папку tmp/'Классификаторы в формате ИМОД для загрузки в БД СОДО' и нажмите кнопку 'Далее'."


regime_mode=$(zenity --width=570 --height=220 --list \
                    --title="Имя БД, в которую будут загружены классификаторы." \
                    --column="" --column="" \
                      1 "Загрузка в базу данных sodo_test" \
                      2 "Загрузка в базу данных sodo" \
                      3 "Загрузка в базу данных sodo_uch" \
                      4 "Загрузка в базу данных sodo_test_uch" \
                )
regime_mode_choice=$?                
if [[ $regime_mode_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi




#-------------------------------------------------------------------------------------------------------------------------------------------------------------




# Ввод данных 
host_address_name=$(zenity --width=200 --height=120 --entry --title="" --text="Введите адрес хоста")
host_address_name_choice=$?
if [[ $host_address_name_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi

base_user_name=$(zenity --width=200 --height=120 --entry --title="" --text='Введите имя пользователя')
user_base_name_choice=$?
if [[ $user_base_name_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi

password_base=$(zenity --width=200 --height=120 --entry --title="" --text='Введите пароль' --hide-text)
password_base_choice=$?
if [[ $password_base_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi

host_port=$(zenity --width=200 --height=120 --entry --title="" --text='Введите порт')
host_port_choice=$?
if [[ $host_port_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi

#-------------------------------------------------------------------------------------------------------------------------------------------------------------



# zenity --width=300 --height=120 --info \
#        --text="Сохраните  классификаторы, предназначенные для загрузки в базу данных СОДО, в папку 'Классификаторы в формате ИМОД для загрузки в БД СОДО' и нажмите кнопку 'ОК'."



cd 'Классификаторы в формате ИМОД для загрузки в БД СОДО'

# ТЕСТОВЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 1)); then 


    array_of_load_scen_files=("scen_load_cl_bg_level_test.xml"  "scen_load_cl_bs_level_test.xml"  "scen_load_cl_charact_actions_test.xml"  "scen_load_cl_character_ootvd_test.xml"  "scen_load_cl_countries_test.xml"  "scen_load_cl_include_types_ros_test.xml"  "scen_load_cl_importance_test.xml"  "scen_load_cl_nuclear_types_test.xml"  "scen_load_cl_ou_types_test.xml"  "scen_load_cl_vf_types_test.xml"  "scen_load_cl_vid_vs_test.xml"  "scen_load_cl_ootvd_types_test.xml"  "scen_load_cl_vvt_types_test.xml"  "scen_load_cl_vvt_samples_test.xml"  "scen_load_cl_vgd_test.xml"  "scen_load_cl_vgd_units_test.xml"  "scen_load_cl_oceans_test.xml" "scen_load_cl_aqua_test.xml" "scen_load_cl_alliances_test.xml" "scen_load_cl_levels_vf_test.xml" "scen_load_cl_rod_test.xml" "scen_load_cl_eop_test.xml" "scen_load_cl_levels_gr_test.xml" "scen_load_cl_sostav_gr_test.xml" "scen_load_cl_scales_gr_test.xml" "scen_load_cl_predn_gr_test.xml" "scen_load_sl_prizn_razv_test.xml")


    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_load_scen_files[@]}; do

        # ищем тег с db_name в сценарии
        host_value=$(grep "<$host_name>.*<.$host_name>" $xml_file | sed -e "s/^.*<$host_name/<$host_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_user в сценарии
        user_value=$(grep "<$user_name>.*<.$user_name>" $xml_file | sed -e "s/^.*<$user_name/<$user_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_pass в сценарии
        password_value=$(grep "<$password>.*<.$password>" $xml_file | sed -e "s/^.*<$password/<$password/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_port в сценарии
        port_value=$(grep "<$port>.*<.$port>" $xml_file | sed -e "s/^.*<$port/<$port/" | cut -f2 -d">"| cut -f1 -d"<")

        # переписываем значение внутри тега db_host в сценарии
        sed -e "s/<$host_name>$host_value<\/$host_name>/<$host_name>$host_address_name<\/$host_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_user в сценарии
        sed -e "s/<$user_name>$user_value<\/$user_name>/<$user_name>$base_user_name<\/$user_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_pass в сценарии
        sed -e "s/<$password>$password_value<\/$password>/<$password>$password_base<\/$password>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_port в сценарии
        sed -e "s/<$port>$port_value<\/$port>/<$port>$host_port<\/$port>/g" $xml_file > $temporary
        mv $temporary $xml_file

    done



    zenity --question --width=300 --height=120 \
        --text="Начать загрузку классификаторов в формате ИМОД в Базу данных sodo_test?" \
        --ok-label="Да" \
        --cancel-label="Нет"

    download_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $download_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_load () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс загрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }


        ./classloader -ng -s scen_load_cl_bg_level_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеготовности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_bs_level_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Степени боевой способности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеспособности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_charact_actions_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Характер действий'.xml" & progress_load "Загрузка Классификатора 'Характер действий'"


        ./classloader -ng -s scen_load_cl_character_ootvd_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Характеристики объектов учета оперативного оборудования театра военных действий'.xml" & progress_load "Загрузка Классификатора 'Характеристики объектов учета оперативного оборудования театра военных действий'"


        ./classloader -ng -s scen_load_cl_countries_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Страны мира ИГ'.xml" & progress_load "Загрузка Классификатора 'Страны мира'"


        ./classloader -ng -s scen_load_cl_include_types_ros_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды оргподчиненности формирований ВС'.xml" & progress_load "Загрузка Классификатора 'Виды оргподчиненности формирований ВС'"


        ./classloader -ng -s scen_load_cl_importance_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки важности объектов учета'.xml" & progress_load "Загрузка Классификатора 'Признаки важности объектов учета'"


        ./classloader -ng -s scen_load_cl_nuclear_types_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки носителя ядерного оружия ИГ'.xml" & progress_load "Загрузка Классификатора 'Признаки носителя ядерного оружия ИГ'"


        ./classloader -ng -s scen_load_cl_ou_types_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды объектов учета'.xml" & progress_load "Загрузка Классификатора 'Виды объектов учета'"


        ./classloader -ng -s scen_load_cl_vf_types_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы формирований ВС ИГ'.xml" & progress_load "Загрузка Классификатора 'Типы формирований ВС ИГ'"


        ./classloader -ng -s scen_load_cl_vid_vs_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды вооруженных сил'.xml" & progress_load "Загрузка Классификатора 'Виды вооруженных сил'"


        ./classloader -ng -s scen_load_cl_ootvd_types_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы объектов оперативного оборудования театра военных действий'.xml" & progress_load "Загрузка Классификатора 'Типы объектов оперативного оборудования театра военных действий'"


        ./classloader -ng -s scen_load_cl_vvt_types_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы вооружения и военной техники'.xml" & progress_load "Загрузка Классификатора 'Типы вооружения и военной техники'"


        ./classloader -ng -s scen_load_cl_vvt_samples_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Конкретные образцы вооружения и военной техники'.xml" & progress_load "Загрузка Классификатора 'Конкретные образцы вооружения и военной техники'"


        ./classloader -ng -s scen_load_cl_vgd_units_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Единиц военно-географического деления'.xml" & progress_load "Загрузка Классификатора 'Единиц военно-географического деления'"


        ./classloader -ng -s scen_load_cl_oceans_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Названия океанов'.xml" & progress_load "Загрузка Классификатора 'Названия океанов'"


        ./classloader -ng -s scen_load_cl_aqua_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Акватории'.xml" & progress_load "Загрузка Классификатора 'Акватории'"


        ./classloader -ng -s scen_load_cl_alliances_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Названия блоков'.xml" & progress_load "Загрузка Классификатора 'Названия блоков'"


        ./classloader -ng -s scen_load_cl_levels_vf_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Уровни воинских формирований'.xml" & progress_load "Загрузка Классификатора 'Уровни воинских формирований'"


        ./classloader -ng -s scen_load_cl_rod_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Рода войск'.xml" & progress_load "Загрузка Классификатора 'Рода войск'"
    

        ./classloader -ng -s scen_load_cl_eop_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Элементы оперативного построения (боевого порядка) объектов противника'.xml" & progress_load "Загрузка Классификатора 'Элементы оперативного построения (боевого порядка) объектов противника'"


        ./classloader -ng -s scen_load_cl_levels_gr_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Уровни группировок'.xml" & progress_load "Загрузка Классификатора 'Уровни группировок'"
    

        ./classloader -ng -s scen_load_cl_sostav_gr_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Составы группировок'.xml" & progress_load "Загрузка Классификатора 'Составы группировок'"
    

        ./classloader -ng -s scen_load_cl_scales_gr_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Масштабы группировок'.xml" & progress_load "Загрузка Классификатора 'Масштабы группировок'"


        ./classloader -ng -s scen_load_cl_predn_gr_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Предназначения группировок'.xml" & progress_load "Загрузка Классификатора 'Предназначения группировок'"

        ./classloader -ng -s scen_load_sl_prizn_razv_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки объекта разведки и поражения'.xml"  & progress_load "Загрузка Классификатора 'Признаки объекта разведки и поражения'"
        

        ./classloader -ng -s scen_load_cl_vgd_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Военно-географическое деление'.xml" & progress_load "Загрузка Классификатора 'Военно-географическое деление'"

        
        
            
        zenity --question --width=300 --height=120 \
        --text="Для последующей загрузки ТПК нажмите кнопку Далее или Отмена для выхода." \
        --ok-label="Далее" \
        --cancel-label="Отмена"
        
        tpk_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

        if [[ $tpk_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
            exit

        else
            export host_address_name
            export base_user_name
            export password_base
            export host_port
            cd ../../"Загрузка ТПК"/enemy; sh tpk_loader_enemy.sh
            
        fi
    fi
fi









# БОЕВЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 2)); then 


    array_of_load_scen_files=("scen_load_cl_bg_level_battle.xml"  "scen_load_cl_bs_level_battle.xml"  "scen_load_cl_charact_actions_battle.xml"  "scen_load_cl_character_ootvd_battle.xml"  "scen_load_cl_countries_battle.xml"  "scen_load_cl_include_types_ros_battle.xml"  "scen_load_cl_importance_battle.xml"  "scen_load_cl_nuclear_types_battle.xml"  "scen_load_cl_ou_types_battle.xml"  "scen_load_cl_vf_types_battle.xml"  "scen_load_cl_vid_vs_battle.xml"  "scen_load_cl_ootvd_types_battle.xml"  "scen_load_cl_vvt_types_battle.xml"  "scen_load_cl_vvt_samples_battle.xml"  "scen_load_cl_vgd_battle.xml"  "scen_load_cl_vgd_units_battle.xml"  "scen_load_cl_oceans_battle.xml" "scen_load_cl_aqua_battle.xml" "scen_load_cl_alliances_battle.xml" "scen_load_cl_levels_vf_battle.xml" "scen_load_cl_rod_battle.xml" "scen_load_cl_eop_battle.xml" "scen_load_cl_levels_gr_battle.xml" "scen_load_cl_sostav_gr_battle.xml" "scen_load_cl_scales_gr_battle.xml" "scen_load_cl_predn_gr_battle.xml" "scen_load_sl_prizn_razv_battle.xml")


    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_load_scen_files[@]}; do

        # ищем тег с db_host в сценарии
        host_value=$(grep "<$host_name>.*<.$host_name>" $xml_file | sed -e "s/^.*<$host_name/<$host_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_user в сценарии
        user_value=$(grep "<$user_name>.*<.$user_name>" $xml_file | sed -e "s/^.*<$user_name/<$user_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_pass в сценарии
        password_value=$(grep "<$password>.*<.$password>" $xml_file | sed -e "s/^.*<$password/<$password/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_port в сценарии
        port_value=$(grep "<$port>.*<.$port>" $xml_file | sed -e "s/^.*<$port/<$port/" | cut -f2 -d">"| cut -f1 -d"<")
        
        # переписываем значение внутри тега db_host в сценарии
        sed -e "s/<$host_name>$host_value<\/$host_name>/<$host_name>$host_address_name<\/$host_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_user в сценарии
        sed -e "s/<$user_name>$user_value<\/$user_name>/<$user_name>$base_user_name<\/$user_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_pass в сценарии
        sed -e "s/<$password>$password_value<\/$password>/<$password>$password_base<\/$password>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_port в сценарии
        sed -e "s/<$port>$port_value<\/$port>/<$port>$host_port<\/$port>/g" $xml_file > $temporary
        mv $temporary $xml_file

    done



    zenity --question --width=300 --height=120 \
        --text="Начать загрузку классификаторов в формате ИМОД в Базу данных sodo?" \
        --ok-label="Да" \
        --cancel-label="Нет"

    download_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $download_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_load () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс загрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }


        ./classloader -ng -s scen_load_cl_bg_level_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеготовности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_bs_level_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Степени боевой способности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеспособности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_charact_actions_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Характер действий'.xml" & progress_load "Загрузка Классификатора 'Характер действий'"


        ./classloader -ng -s scen_load_cl_character_ootvd_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Характеристики объектов учета оперативного оборудования театра военных действий'.xml" & progress_load "Загрузка Классификатора 'Характеристики объектов учета оперативного оборудования театра военных действий'"


        ./classloader -ng -s scen_load_cl_countries_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Страны мира ИГ'.xml" & progress_load "Загрузка Классификатора 'Страны мира'"


        ./classloader -ng -s scen_load_cl_include_types_ros_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды оргподчиненности формирований ВС'.xml" & progress_load "Загрузка Классификатора 'Виды оргподчиненности формирований ВС'"


        ./classloader -ng -s scen_load_cl_importance_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки важности объектов учета'.xml" & progress_load "Загрузка Классификатора 'Признаки важности объектов учета'"


        ./classloader -ng -s scen_load_cl_nuclear_types_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки носителя ядерного оружия ИГ'.xml" & progress_load "Загрузка Классификатора 'Признаки носителя ядерного оружия ИГ'"


        ./classloader -ng -s scen_load_cl_ou_types_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды объектов учета'.xml" & progress_load "Загрузка Классификатора 'Виды объектов учета'"


        ./classloader -ng -s scen_load_cl_vf_types_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы формирований ВС ИГ'.xml" & progress_load "Загрузка Классификатора 'Типы формирований ВС ИГ'"


        ./classloader -ng -s scen_load_cl_vid_vs_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды вооруженных сил'.xml" & progress_load "Загрузка Классификатора 'Виды вооруженных сил'"


        ./classloader -ng -s scen_load_cl_ootvd_types_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы объектов оперативного оборудования театра военных действий'.xml" & progress_load "Загрузка Классификатора 'Типы объектов оперативного оборудования театра военных действий'"


        ./classloader -ng -s scen_load_cl_vvt_types_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы вооружения и военной техники'.xml" & progress_load "Загрузка Классификатора 'Типы вооружения и военной техники'"


        ./classloader -ng -s scen_load_cl_vvt_samples_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Конкретные образцы вооружения и военной техники'.xml" & progress_load "Загрузка Классификатора 'Конкретные образцы вооружения и военной техники'"


        ./classloader -ng -s scen_load_cl_vgd_units_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Единиц военно-географического деления'.xml" & progress_load "Загрузка Классификатора 'Единиц военно-географического деления'"


        ./classloader -ng -s scen_load_cl_oceans_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Названия океанов'.xml" & progress_load "Загрузка Классификатора 'Названия океанов'"


        ./classloader -ng -s scen_load_cl_aqua_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Акватории'.xml" & progress_load "Загрузка Классификатора 'Акватории'"


        ./classloader -ng -s scen_load_cl_alliances_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Названия блоков'.xml" & progress_load "Загрузка Классификатора 'Названия блоков'"


        ./classloader -ng -s scen_load_cl_levels_vf_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Уровни воинских формирований'.xml" & progress_load "Загрузка Классификатора 'Уровни воинских формирований'"


        ./classloader -ng -s scen_load_cl_rod_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Рода войск'.xml" & progress_load "Загрузка Классификатора 'Рода войск'"
    

        ./classloader -ng -s scen_load_cl_eop_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Элементы оперативного построения (боевого порядка) объектов противника'.xml" & progress_load "Загрузка Классификатора 'Элементы оперативного построения (боевого порядка) объектов противника'"


        ./classloader -ng -s scen_load_cl_levels_gr_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Уровни группировок'.xml" & progress_load "Загрузка Классификатора 'Уровни группировок'"
    

        ./classloader -ng -s scen_load_cl_sostav_gr_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Составы группировок'.xml" & progress_load "Загрузка Классификатора 'Составы группировок'"
    

        ./classloader -ng -s scen_load_cl_scales_gr_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Масштабы группировок'.xml" & progress_load "Загрузка Классификатора 'Масштабы группировок'"


        ./classloader -ng -s scen_load_cl_predn_gr_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Предназначения группировок'.xml" & progress_load "Загрузка Классификатора 'Предназначения группировок'"

        ./classloader -ng -s scen_load_sl_prizn_razv_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки объекта разведки и поражения'.xml"  & progress_load "Загрузка Классификатора 'Признаки объекта разведки и поражения'"
        

        ./classloader -ng -s scen_load_cl_vgd_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Военно-географическое деление'.xml" & progress_load "Загрузка Классификатора 'Военно-географическое деление'"



    fi
fi







# УЧЕБНЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 3)); then 


    array_of_load_scen_files=("scen_load_cl_bg_level_uch.xml"  "scen_load_cl_bs_level_uch.xml"  "scen_load_cl_charact_actions_uch.xml"  "scen_load_cl_character_ootvd_uch.xml"  "scen_load_cl_countries_uch.xml"  "scen_load_cl_include_types_ros_uch.xml"  "scen_load_cl_importance_uch.xml"  "scen_load_cl_nuclear_types_uch.xml"  "scen_load_cl_ou_types_uch.xml"  "scen_load_cl_vf_types_uch.xml"  "scen_load_cl_vid_vs_uch.xml"  "scen_load_cl_ootvd_types_uch.xml"  "scen_load_cl_vvt_types_uch.xml"  "scen_load_cl_vvt_samples_uch.xml"  "scen_load_cl_vgd_uch.xml"  "scen_load_cl_vgd_units_uch.xml"  "scen_load_cl_oceans_uch.xml" "scen_load_cl_aqua_uch.xml" "scen_load_cl_alliances_uch.xml" "scen_load_cl_levels_vf_uch.xml" "scen_load_cl_rod_uch.xml" "scen_load_cl_eop_uch.xml" "scen_load_cl_levels_gr_uch.xml" "scen_load_cl_sostav_gr_uch.xml" "scen_load_cl_scales_gr_uch.xml" "scen_load_cl_predn_gr_uch.xml" "scen_load_sl_prizn_razv_uch.xml")


    base_name="db_name"
    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_load_scen_files[@]}; do

        # ищем тег с db_host в сценарии
        host_value=$(grep "<$host_name>.*<.$host_name>" $xml_file | sed -e "s/^.*<$host_name/<$host_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_user в сценарии
        user_value=$(grep "<$user_name>.*<.$user_name>" $xml_file | sed -e "s/^.*<$user_name/<$user_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_pass в сценарии
        password_value=$(grep "<$password>.*<.$password>" $xml_file | sed -e "s/^.*<$password/<$password/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_port в сценарии
        port_value=$(grep "<$port>.*<.$port>" $xml_file | sed -e "s/^.*<$port/<$port/" | cut -f2 -d">"| cut -f1 -d"<")
        
        
        # переписываем значение внутри тега db_host в сценарии
        sed -e "s/<$host_name>$host_value<\/$host_name>/<$host_name>$host_address_name<\/$host_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_user в сценарии
        sed -e "s/<$user_name>$user_value<\/$user_name>/<$user_name>$base_user_name<\/$user_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_pass в сценарии
        sed -e "s/<$password>$password_value<\/$password>/<$password>$password_base<\/$password>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_port в сценарии
        sed -e "s/<$port>$port_value<\/$port>/<$port>$host_port<\/$port>/g" $xml_file > $temporary
        mv $temporary $xml_file

    done



    zenity --question --width=300 --height=120 \
        --text="Начать загрузку классификаторов в формате ИМОД в Базу данных sodo_uch?" \
        --ok-label="Да" \
        --cancel-label="Нет"

    download_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $download_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_load () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс загрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }


        ./classloader -ng -s scen_load_cl_bg_level_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеготовности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_bs_level_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Степени боевой способности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеспособности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_charact_actions_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Характер действий'.xml" & progress_load "Загрузка Классификатора 'Характер действий'"

        ./classloader -ng -s scen_load_cl_character_ootvd_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Характеристики объектов учета оперативного оборудования театра военных действий'.xml" & progress_load "Загрузка Классификатора 'Характеристики объектов учета оперативного оборудования театра военных действий'"

        ./classloader -ng -s scen_load_cl_countries_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Страны мира ИГ'.xml" & progress_load "Загрузка Классификатора 'Страны мира'"


        ./classloader -ng -s scen_load_cl_include_types_ros_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды оргподчиненности формирований ВС'.xml" & progress_load "Загрузка Классификатора 'Виды оргподчиненности формирований ВС'"


        ./classloader -ng -s scen_load_cl_importance_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки важности объектов учета'.xml" & progress_load "Загрузка Классификатора 'Признаки важности объектов учета'"


        ./classloader -ng -s scen_load_cl_nuclear_types_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки носителя ядерного оружия ИГ'.xml" & progress_load "Загрузка Классификатора 'Признаки носителя ядерного оружия ИГ'"


        ./classloader -ng -s scen_load_cl_ou_types_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды объектов учета'.xml" & progress_load "Загрузка Классификатора 'Виды объектов учета'"


        ./classloader -ng -s scen_load_cl_vf_types_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы формирований ВС ИГ'.xml" & progress_load "Загрузка Классификатора 'Типы формирований ВС ИГ'"


        ./classloader -ng -s scen_load_cl_vid_vs_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды вооруженных сил'.xml" & progress_load "Загрузка Классификатора 'Виды вооруженных сил'"


        ./classloader -ng -s scen_load_cl_ootvd_types_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы объектов оперативного оборудования театра военных действий'.xml" & progress_load "Загрузка Классификатора 'Типы объектов оперативного оборудования театра военных действий'"


        ./classloader -ng -s scen_load_cl_vvt_types_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы вооружения и военной техники'.xml" & progress_load "Загрузка Классификатора 'Типы вооружения и военной техники'"


        ./classloader -ng -s scen_load_cl_vvt_samples_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Конкретные образцы вооружения и военной техники'.xml" & progress_load "Загрузка Классификатора 'Конкретные образцы вооружения и военной техники'"


        ./classloader -ng -s scen_load_cl_vgd_units_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Единиц военно-географического деления'.xml" & progress_load "Загрузка Классификатора 'Единиц военно-географического деления'"


        ./classloader -ng -s scen_load_cl_oceans_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Названия океанов'.xml" & progress_load "Загрузка Классификатора 'Названия океанов'"


        ./classloader -ng -s scen_load_cl_aqua_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Акватории'.xml" & progress_load "Загрузка Классификатора 'Акватории'"


        ./classloader -ng -s scen_load_cl_alliances_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Названия блоков'.xml" & progress_load "Загрузка Классификатора 'Названия блоков'"


        ./classloader -ng -s scen_load_cl_levels_vf_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Уровни воинских формирований'.xml" & progress_load "Загрузка Классификатора 'Уровни воинских формирований'"


        ./classloader -ng -s scen_load_cl_rod_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Рода войск'.xml" & progress_load "Загрузка Классификатора 'Рода войск'"
    

        ./classloader -ng -s scen_load_cl_eop_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Элементы оперативного построения (боевого порядка) объектов противника'.xml" & progress_load "Загрузка Классификатора 'Элементы оперативного построения (боевого порядка) объектов противника'"


        ./classloader -ng -s scen_load_cl_levels_gr_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Уровни группировок'.xml" & progress_load "Загрузка Классификатора 'Уровни группировок'"
    

        ./classloader -ng -s scen_load_cl_sostav_gr_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Составы группировок'.xml" & progress_load "Загрузка Классификатора 'Составы группировок'"
    

        ./classloader -ng -s scen_load_cl_scales_gr_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Масштабы группировок'.xml" & progress_load "Загрузка Классификатора 'Масштабы группировок'"


        ./classloader -ng -s scen_load_cl_predn_gr_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Предназначения группировок'.xml" & progress_load "Загрузка Классификатора 'Предназначения группировок'"

        ./classloader -ng -s scen_load_sl_prizn_razv_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки объекта разведки и поражения'.xml"  & progress_load "Загрузка Классификатора 'Признаки объекта разведки и поражения'"
        

        ./classloader -ng -s scen_load_cl_vgd_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Военно-географическое деление'.xml" & progress_load "Загрузка Классификатора 'Военно-географическое деление'"


    fi
fi



if (($regime_mode == 4)); then 


    array_of_load_scen_files=("scen_load_cl_bg_level_test_uch.xml"  "scen_load_cl_bs_level_test_uch.xml"  "scen_load_cl_charact_actions_test_uch.xml"  "scen_load_cl_character_ootvd_test_uch.xml"  "scen_load_cl_countries_test_uch.xml"  "scen_load_cl_include_types_ros_test_uch.xml"  "scen_load_cl_importance_test_uch.xml"  "scen_load_cl_nuclear_types_test_uch.xml"  "scen_load_cl_ou_types_test_uch.xml"  "scen_load_cl_vf_types_test_uch.xml"  "scen_load_cl_vid_vs_test_uch.xml"  "scen_load_cl_ootvd_types_test_uch.xml"  "scen_load_cl_vvt_types_test_uch.xml"  "scen_load_cl_vvt_samples_test_uch.xml"  "scen_load_cl_vgd_test_uch.xml"  "scen_load_cl_vgd_units_test_uch.xml"  "scen_load_cl_oceans_test_uch.xml" "scen_load_cl_aqua_test_uch.xml" "scen_load_cl_alliances_test_uch.xml" "scen_load_cl_levels_vf_test_uch.xml" "scen_load_cl_rod_test_uch.xml" "scen_load_cl_eop_test_uch.xml" "scen_load_cl_levels_gr_test_uch.xml" "scen_load_cl_sostav_gr_test_uch.xml" "scen_load_cl_scales_gr_test_uch.xml" "scen_load_cl_predn_gr_test_uch.xml" "scen_load_sl_prizn_razv_test_uch.xml")


    base_name="db_name"
    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_load_scen_files[@]}; do

        # ищем тег с db_host в сценарии
        host_value=$(grep "<$host_name>.*<.$host_name>" $xml_file | sed -e "s/^.*<$host_name/<$host_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_user в сценарии
        user_value=$(grep "<$user_name>.*<.$user_name>" $xml_file | sed -e "s/^.*<$user_name/<$user_name/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_pass в сценарии
        password_value=$(grep "<$password>.*<.$password>" $xml_file | sed -e "s/^.*<$password/<$password/" | cut -f2 -d">"| cut -f1 -d"<")
        # ищем тег с db_port в сценарии
        port_value=$(grep "<$port>.*<.$port>" $xml_file | sed -e "s/^.*<$port/<$port/" | cut -f2 -d">"| cut -f1 -d"<")
        
        
        # переписываем значение внутри тега db_host в сценарии
        sed -e "s/<$host_name>$host_value<\/$host_name>/<$host_name>$host_address_name<\/$host_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_user в сценарии
        sed -e "s/<$user_name>$user_value<\/$user_name>/<$user_name>$base_user_name<\/$user_name>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_pass в сценарии
        sed -e "s/<$password>$password_value<\/$password>/<$password>$password_base<\/$password>/g" $xml_file > $temporary
        mv $temporary $xml_file

        # переписываем значение внутри тега db_port в сценарии
        sed -e "s/<$port>$port_value<\/$port>/<$port>$host_port<\/$port>/g" $xml_file > $temporary
        mv $temporary $xml_file

    done



    zenity --question --width=300 --height=120 \
        --text="Начать загрузку классификаторов в формате ИМОД в Базу данных sodo_test_uch?" \
        --ok-label="Да" \
        --cancel-label="Нет"

    download_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $download_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_load () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс загрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }


        ./classloader -ng -s scen_load_cl_bg_level_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеготовности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_bs_level_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Степени боевой способности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеспособности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_charact_actions_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Характер действий'.xml" & progress_load "Загрузка Классификатора 'Характер действий'"


        ./classloader -ng -s scen_load_cl_character_ootvd_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Характеристики объектов учета оперативного оборудования театра военных действий'.xml" & progress_load "Загрузка Классификатора 'Характеристики объектов учета оперативного оборудования театра военных действий'"


        ./classloader -ng -s scen_load_cl_countries_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Страны мира ИГ'.xml" & progress_load "Загрузка Классификатора 'Страны мира'"


        ./classloader -ng -s scen_load_cl_include_types_ros_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды оргподчиненности формирований ВС'.xml" & progress_load "Загрузка Классификатора 'Виды оргподчиненности формирований ВС'"


        ./classloader -ng -s scen_load_cl_importance_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки важности объектов учета'.xml" & progress_load "Загрузка Классификатора 'Признаки важности объектов учета'"


        ./classloader -ng -s scen_load_cl_nuclear_types_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки носителя ядерного оружия ИГ'.xml" & progress_load "Загрузка Классификатора 'Признаки носителя ядерного оружия ИГ'"


        ./classloader -ng -s scen_load_cl_ou_types_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды объектов учета'.xml" & progress_load "Загрузка Классификатора 'Виды объектов учета'"


        ./classloader -ng -s scen_load_cl_vf_types_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы формирований ВС ИГ'.xml" & progress_load "Загрузка Классификатора 'Типы формирований ВС ИГ'"


        ./classloader -ng -s scen_load_cl_vid_vs_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Виды вооруженных сил'.xml" & progress_load "Загрузка Классификатора 'Виды вооруженных сил'"


        ./classloader -ng -s scen_load_cl_ootvd_types_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы объектов оперативного оборудования театра военных действий'.xml" & progress_load "Загрузка Классификатора 'Типы объектов оперативного оборудования театра военных действий'"


        ./classloader -ng -s scen_load_cl_vvt_types_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы вооружения и военной техники'.xml" & progress_load "Загрузка Классификатора 'Типы вооружения и военной техники'"


        ./classloader -ng -s scen_load_cl_vvt_samples_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Конкретные образцы вооружения и военной техники'.xml" & progress_load "Загрузка Классификатора 'Конкретные образцы вооружения и военной техники'"


        ./classloader -ng -s scen_load_cl_vgd_units_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Единиц военно-географического деления'.xml" & progress_load "Загрузка Классификатора 'Единиц военно-географического деления'"


        ./classloader -ng -s scen_load_cl_oceans_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Названия океанов'.xml" & progress_load "Загрузка Классификатора 'Названия океанов'"


        ./classloader -ng -s scen_load_cl_aqua_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Акватории'.xml" & progress_load "Загрузка Классификатора 'Акватории'"


        ./classloader -ng -s scen_load_cl_alliances_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Названия блоков'.xml" & progress_load "Загрузка Классификатора 'Названия блоков'"


        ./classloader -ng -s scen_load_cl_levels_vf_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Уровни воинских формирований'.xml" & progress_load "Загрузка Классификатора 'Уровни воинских формирований'"


        ./classloader -ng -s scen_load_cl_rod_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Рода войск'.xml" & progress_load "Загрузка Классификатора 'Рода войск'"
    

        ./classloader -ng -s scen_load_cl_eop_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Элементы оперативного построения (боевого порядка) объектов противника'.xml" & progress_load "Загрузка Классификатора 'Элементы оперативного построения (боевого порядка) объектов противника'"


        ./classloader -ng -s scen_load_cl_levels_gr_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Уровни группировок'.xml" & progress_load "Загрузка Классификатора 'Уровни группировок'"
    

        ./classloader -ng -s scen_load_cl_sostav_gr_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Составы группировок'.xml" & progress_load "Загрузка Классификатора 'Составы группировок'"
    

        ./classloader -ng -s scen_load_cl_scales_gr_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Масштабы группировок'.xml" & progress_load "Загрузка Классификатора 'Масштабы группировок'"


        ./classloader -ng -s scen_load_cl_predn_gr_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Предназначения группировок'.xml" & progress_load "Загрузка Классификатора 'Предназначения группировок'"

        ./classloader -ng -s scen_load_sl_prizn_razv_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Признаки объекта разведки и поражения'.xml"  & progress_load "Загрузка Классификатора 'Признаки объекта разведки и поражения'"
        

        ./classloader -ng -s scen_load_cl_vgd_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Военно-географическое деление'.xml" & progress_load "Загрузка Классификатора 'Военно-географическое деление'"



    fi
fi


zenity --question --width=300 --height=120 \
--text="Классификаторы успешно загружены. Для перехода в главное меню нажмите Далее, для выхода из программы нажмите Выход." \
--ok-label="Далее" \
--cancel-label="Выход"

continue_choice=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

if [[ $continue_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
#     exit

else
    cd /opt/rbt/vbd/LOADER; sh start.sh
fi
