#!/bin/bash


array_of_load_scen_files=("scen_tpk_load_cl_state_names_st_name_guid.xml" "scen_tpk_load_cl_state_names_org_names.xml" "scen_tpk_load_cl_vvt_types.xml")

host_name="db_host"
user_name="db_user"
password="db_pass"
port="db_port"



# host_address_name=$(zenity --width=200 --height=120 --entry --title="" --text="Введите адрес хоста")
# host_address_name_choice=$?
# if [[ $host_address_name_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
#     exit
# fi
# 
# base_user_name=$(zenity --width=200 --height=120 --entry --title="" --text='Введите имя пользователя')
# user_base_name_choice=$?
# if [[ $user_base_name_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
#     exit
# fi
# 
# password_base=$(zenity --width=200 --height=120 --entry --title="" --text='Введите пароль')
# password_base_choice=$?
# if [[ $password_base_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
#     exit
# fi
# 
# host_port=$(zenity --width=200 --height=120 --entry --title="" --text='Введите порт')
# host_port_choice=$?
# if [[ $host_port_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
#     exit
# fi

for xml_file in ${array_of_load_scen_files[@]}; do

    xmlstarlet ed -L -u "/scenario/db_host" -v $host_address_name $xml_file #host
    xmlstarlet ed -L -u "/scenario/db_name" -v $base_user_name $xml_file  #user

done    


progress_load () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс загрузки ТПК...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }

./classloader -ng -s scen_tpk_load_cl_state_names_st_name_guid.xml -d "ТПК [красные] 'Типы ВФ' (Часть 1)-18.01.2022.xml" & progress_load "Загрузка ТПК 'Штатов и структурных подразделений'"
./classloader -ng -s scen_tpk_load_cl_state_names_st_name_guid.xml -d "ТПК [красные] 'Типы ВФ' (Часть 2)-18.01.2022.xml" & progress_load "Загрузка ТПК 'Штатов и структурных подразделений'"
./classloader -ng -s scen_tpk_load_cl_state_names_st_name_guid.xml -d "ТПК [красные] [СОДО] Классификатор 'НШСП'-'Типы воинских формирований'-17.08.2022.xml" & progress_load "Загрузка ТПК 'Штатов и структурных подразделений'"
./classloader -ng -s scen_tpk_load_cl_state_names_org_names.xml -d "ТПК [красные] КНШСП - Организационные наименования.xml" & progress_load "Загрузка ТПК 'Штатов и структурных подразделений'"


./classloader -ng -s scen_tpk_load_cl_vvt_types.xml -d "ТПК [красные] 'Типы ВВСТ' (Часть 1)-18.01.2022.xml" & progress_load "Загрузка ТПК 'Вооружение, военная техника и другие материальные средства Министерства обороны'."
./classloader -ng -s scen_tpk_load_cl_vvt_types.xml -d "ТПК [красные] 'Типы ВВСТ' (Часть 2)-18.01.2022.xml" & progress_load "Загрузка ТПК 'Вооружение, военная техника и другие материальные средства Министерства обороны'."
./classloader -ng -s scen_tpk_load_cl_vvt_types.xml -d "ТПК [красные] 'Типы ВВСТ' (Часть 3)-18.01.2022.xml" & progress_load "Загрузка ТПК 'Вооружение, военная техника и другие материальные средства Министерства обороны'."
./classloader -ng -s scen_tpk_load_cl_vvt_types.xml -d "ТПК [красные] [СОДО] Классификатор 'Типы ВВСТ РФ' и Рубрикатор 'Типы воинских формирований'-17.08.2022.xml" & progress_load "Загрузка ТПК 'Вооружение, военная техника и другие материальные средства Министерства обороны'."
./classloader -ng -s scen_tpk_load_cl_vvt_types.xml -d "ТПК [красные] [СОДО] Классификатор 'Типы ВВСТ РФ' и Рубрикатор 'Типы вооружения, военной и специальной техники'-17.08.2022.xml" & progress_load "Загрузка ТПК 'Вооружение, военная техника и другие материальные средства Министерства обороны'."


zenity --question --width=300 --height=120 \
--text="ТПК успешно загружены. Для перехода в главное меню нажмите Далее, для выхода из программы нажмите Выход." \
--ok-label="Далее" \
--cancel-label="Выход"

continue_choice=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

if [[ $continue_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit

else
    cd /opt/rbt/vbd/LOADER; sh start.sh
fi
