#!/bin/bash

mkdir /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"

zenity --width=300 --height=120 --info \
        --ok-label='Далее' \
        --text="Классификаторы, для загрузки в БД УПИ, находятся в папке tmp/'Классификаторы в формате ИМОД'. Загрузите имеющиеся классификаторы в БД УПИ средствами КП ИЛО и нажмите кнопку 'Далее'."

zenity --width=300 --height=120 --info \
        --ok-label='Далее' \
        --text="Выгрузите классификаторы из БД УПИ средствами КП ИЛО, сохраните полученные классификаторы в папку tmp/'Классификаторы в формате ИМОД для загрузки в БД СОДО' и нажмите кнопку 'Далее'."




regime_mode=$(zenity --width=570 --height=220 --list \
                    --title="Имя БД, в которую будут загружены классификаторы." \
                    --column="" --column="" \
                      1 "Загрузка в базу данных sodo_test" \
                      2 "Загрузка в базу данных sodo" \
                      3 "Загрузка в базу данных sodo_uch" \
                      4 "Загрузка в базу данных sodo_test_uch" \
                )
regime_mode_choice=$?                
if [[ $regime_mode_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi




host_address_name=$(zenity --width=200 --height=120 --entry --title="" --text="Введите адрес хоста")
host_address_name_choice=$?
if [[ $host_address_name_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi

base_user_name=$(zenity --width=200 --height=120 --entry --title="" --text='Введите имя пользователя')
user_base_name_choice=$?
if [[ $user_base_name_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi

password_base=$(zenity --width=200 --height=120 --entry --title="" --text='Введите пароль' --hide-text)
password_base_choice=$?
if [[ $password_base_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi

host_port=$(zenity --width=200 --height=120 --entry --title="" --text='Введите порт')
host_port_choice=$?
if [[ $host_port_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit
fi






#-------------------------------------------------------------------------------------------------------------------------------------------------------------

# zenity --width=300 --height=120 --info \
#        --text="Сохраните  классификаторы, предназначенные для загрузки в базу данных СОДО, в папку 'Классификаторы в формате ИМОД для загрузки в БД СОДО' и нажмите кнопку 'ОК'."


cd 'Классификаторы в формате ИМОД для загрузки в БД СОДО'




# ТЕСТОВЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 1)); then 
    array_of_load_scen_files=( "scen_load_cl_bg_level_test.xml" "scen_load_cl_garnizon_test.xml" "scen_load_cl_gs_test.xml"  "scen_load_cl_mob_events_test.xml" "scen_load_cl_okrug_flot_test.xml"  "scen_load_cl_osoben_test.xml"  "scen_load_cl_pd_test.xml" "scen_load_cl_posts_gp_test.xml" "scen_load_cl_posts_off_test.xml" "scen_load_cl_posts_sold_test.xml" "scen_load_cl_ppv_gd_test.xml" "scen_load_cl_prednaz_test.xml"  "scen_load_cl_rod_vs_test.xml" "scen_load_cl_ship_types_test.xml" "scen_load_cl_specialization_test.xml" "scen_load_cl_state_names_test.xml" "scen_load_cl_vid_vs_test.xml" "scen_load_cl_vvt_types_test.xml")

        

    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_load_scen_files[@]}; do

        xmlstarlet ed -L -u "/scenario/db_host" -v $host_address_name $xml_file #host
        xmlstarlet ed -L -u "/scenario/db_name" -v $base_user_name $xml_file  #user

    done

    # После того, как пользователь проделал все шаги, связанные с БД УПИ, классификаторы загружаются в БД
    zenity --question --width=300 --height=120 \
        --text="Начать загрузку классификаторов в формате ИМОД в базу данных sodo_test?" \
        --ok-label="Да" \
        --cancel-label="Нет"
        

    choice=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_load () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс загрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }
        
    # Старт загрузки классификаторов в БД 
        ./classloader -ng -s scen_load_cl_bg_level_test.xml -d "Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеготовности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_vvt_types_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_load "Загрузка классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'."
        
        ./classloader -ng -s scen_load_cl_ship_types_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы кораблей'.xml" & progress_load "Загрузка классификатора 'Типы кораблей'."
        
        ./classloader -ng -s scen_load_cl_garnizon_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень гарнизонов'.xml" & progress_load "Загрузка классификатора 'Перечень гарнизонов'." 
        
        ./classloader -ng -s scen_load_cl_gs_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименований главных слов'.xml" & progress_load "Загрузка классификатора 'Наименований главных слов'."

        ./classloader -ng -s scen_load_cl_mob_events_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_load "Загрузка классификатора 'Мобилизационные мероприятия'."

        ./classloader -ng -s scen_load_cl_okrug_flot_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_load "Загрузка классификатора 'Перечень военных округов, флотов, флотилий'."

        ./classloader -ng -s scen_load_cl_osoben_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования особенностей'.xml" & progress_load "Загрузка классификатора 'Наименования особенностей'."

        ./classloader -ng -s scen_load_cl_pd_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Пункты дислокации'.xml" & progress_load "Загрузка классификатора 'Пункты дислокации'."

        ./classloader -ng -s scen_load_cl_posts_gp_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_load "Загрузка классификатора 'Штатных должностей гражданского персонала'."

        ./classloader -ng -s scen_load_cl_posts_off_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей офицеров'.xml" & progress_load "Загрузка классификатора 'Штатных должностей офицеров'."

        ./classloader -ng -s scen_load_cl_posts_sold_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей солдат'.xml" & progress_load "Загрузка классификатора 'Штатных должностей солдат'."

        ./classloader -ng -s scen_load_cl_ppv_gd_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_load "Загрузка классификатора 'Пунктов погрузки-выгрузки'."

        ./classloader -ng -s scen_load_cl_prednaz_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_load "Загрузка классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'."

        ./classloader -ng -s scen_load_cl_rod_vs_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_load "Загрузка классификатора 'Перечень родов войск, специальных войск и служб'."

        ./classloader -ng -s scen_load_cl_vid_vs_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_load "Загрузка классификатора 'Перечень видов Вооруженных Сил'."

        ./classloader -ng -s scen_load_cl_specialization_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования специализации'.xml" & progress_load "Загрузка классификатора 'Наименования специализации'."
        
        ./classloader -ng -s scen_load_cl_state_names_test.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_load "Загрузка классификатора 'Наименования штатов и структурных подразделений'."
        
        
        
        zenity --question --width=300 --height=120 \
        --text="Для последующей загрузки ТПК нажмите кнопку Далее или Отмена для выхода." \
        --ok-label="Далее" \
        --cancel-label="Отмена"
        
        tpk_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

        if [[ $tpk_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
            exit

        else
            export host_address_name
            export base_user_name
            export password_base
            export host_port
            cd ../../"Загрузка ТПК"/public; sh tpk_loader_public.sh
        fi
    fi
fi



# БОЕВЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 2)); then 
    array_of_load_scen_files=( "scen_load_cl_bg_level_battle.xml" "scen_load_cl_garnizon_battle.xml" "scen_load_cl_gs_battle.xml"  "scen_load_cl_mob_events_battle.xml" "scen_load_cl_okrug_flot_battle.xml"  "scen_load_cl_osoben_battle.xml"  "scen_load_cl_pd_battle.xml" "scen_load_cl_posts_gp_battle.xml" "scen_load_cl_posts_off_battle.xml" "scen_load_cl_posts_sold_battle.xml" "scen_load_cl_ppv_gd_battle.xml" "scen_load_cl_prednaz_battle.xml"  "scen_load_cl_rod_vs_battle.xml" "scen_load_cl_ship_types_battle.xml" "scen_load_cl_specialization_battle.xml" "scen_load_cl_state_names_battle.xml" "scen_load_cl_vid_vs_battle.xml" "scen_load_cl_vvt_types_battle.xml")
        

    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_load_scen_files[@]}; do

        xmlstarlet ed -L -u "/scenario/db_host" -v $host_address_name $xml_file #host
        xmlstarlet ed -L -u "/scenario/db_name" -v $base_user_name $xml_file  #user

    done

    # После того, как пользователь проделал все шаги, связанные с БД УПИ, классификаторы загружаются в БД
    zenity --question --width=300 --height=120 \
        --text="Начать загрузку классификаторов в формате ИМОД в базу данных sodo?" \
        --ok-label="Да" \
        --cancel-label="Нет"
        

    choice=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_load () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс загрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }
        
    # Старт загрузки классификаторов в БД 
        ./classloader -ng -s scen_load_cl_bg_level_battle.xml -d "Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеготовности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_vvt_types_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_load "Загрузка классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'."
        
        ./classloader -ng -s scen_load_cl_ship_types_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы кораблей'.xml" & progress_load "Загрузка классификатора 'Типы кораблей'."
        
        ./classloader -ng -s scen_load_cl_garnizon_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень гарнизонов'.xml" & progress_load "Загрузка классификатора 'Перечень гарнизонов'." 
        
        ./classloader -ng -s scen_load_cl_gs_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименований главных слов'.xml" & progress_load "Загрузка классификатора 'Наименований главных слов'."

        ./classloader -ng -s scen_load_cl_mob_events_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_load "Загрузка классификатора 'Мобилизационные мероприятия'."

        ./classloader -ng -s scen_load_cl_okrug_flot_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_load "Загрузка классификатора 'Перечень военных округов, флотов, флотилий'."

        ./classloader -ng -s scen_load_cl_osoben_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования особенностей'.xml" & progress_load "Загрузка классификатора 'Наименования особенностей'."

        ./classloader -ng -s scen_load_cl_pd_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Пункты дислокации'.xml" & progress_load "Загрузка классификатора 'Пункты дислокации'."

        ./classloader -ng -s scen_load_cl_posts_gp_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_load "Загрузка классификатора 'Штатных должностей гражданского персонала'."

        ./classloader -ng -s scen_load_cl_posts_off_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей офицеров'.xml" & progress_load "Загрузка классификатора 'Штатных должностей офицеров'."

        ./classloader -ng -s scen_load_cl_posts_sold_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей солдат'.xml" & progress_load "Загрузка классификатора 'Штатных должностей солдат'."

        ./classloader -ng -s scen_load_cl_ppv_gd_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_load "Загрузка классификатора 'Пунктов погрузки-выгрузки'."

        ./classloader -ng -s scen_load_cl_prednaz_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_load "Загрузка классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'."

        ./classloader -ng -s scen_load_cl_rod_vs_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_load "Загрузка классификатора 'Перечень родов войск, специальных войск и служб'."

        ./classloader -ng -s scen_load_cl_vid_vs_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_load "Загрузка классификатора 'Перечень видов Вооруженных Сил'."

        ./classloader -ng -s scen_load_cl_specialization_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования специализации'.xml" & progress_load "Загрузка классификатора 'Наименования специализации'."
        
        ./classloader -ng -s scen_load_cl_state_names_battle.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_load "Загрузка классификатора 'Наименования штатов и структурных подразделений'."
        


    fi
fi



# УЧЕБНЫЕ КЛАССИФИКАТОРЫ
if (($regime_mode == 3)); then 
    array_of_load_scen_files=( "scen_load_cl_bg_level_uch.xml" "scen_load_cl_garnizon_uch.xml" "scen_load_cl_gs_uch.xml"  "scen_load_cl_mob_events_uch.xml" "scen_load_cl_okrug_flot_uch.xml"  "scen_load_cl_osoben_uch.xml"  "scen_load_cl_pd_uch.xml" "scen_load_cl_posts_gp_uch.xml" "scen_load_cl_posts_off_uch.xml" "scen_load_cl_posts_sold_uch.xml" "scen_load_cl_ppv_gd_uch.xml" "scen_load_cl_prednaz_uch.xml"  "scen_load_cl_rod_vs_uch.xml" "scen_load_cl_ship_types_uch.xml" "scen_load_cl_specialization_uch.xml" "scen_load_cl_state_names_uch.xml" "scen_load_cl_vid_vs_uch.xml" "scen_load_cl_vvt_types_uch.xml")
        

    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_load_scen_files[@]}; do
    
        xmlstarlet ed -L -u "/scenario/db_host" -v $host_address_name $xml_file #host
        xmlstarlet ed -L -u "/scenario/db_name" -v $base_user_name $xml_file  #user

    done

    # После того, как пользователь проделал все шаги, связанные с БД УПИ, классификаторы загружаются в БД
    zenity --question --width=300 --height=120 \
        --text="Начать загрузку классификаторов в формате ИМОД в базу данных sodo_uch?" \
        --ok-label="Да" \
        --cancel-label="Нет"
        

    choice=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_load () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс загрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }
        
    # Старт загрузки классификаторов в БД 
        ./classloader -ng -s scen_load_cl_bg_level_uch.xml -d "Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеготовности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_vvt_types_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_load "Загрузка классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'."
        
        ./classloader -ng -s scen_load_cl_ship_types_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы кораблей'.xml" & progress_load "Загрузка классификатора 'Типы кораблей'."
        
        ./classloader -ng -s scen_load_cl_garnizon_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень гарнизонов'.xml" & progress_load "Загрузка классификатора 'Перечень гарнизонов'." 
        
        ./classloader -ng -s scen_load_cl_gs_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименований главных слов'.xml" & progress_load "Загрузка классификатора 'Наименований главных слов'."

        ./classloader -ng -s scen_load_cl_mob_events_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_load "Загрузка классификатора 'Мобилизационные мероприятия'."

        ./classloader -ng -s scen_load_cl_okrug_flot_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_load "Загрузка классификатора 'Перечень военных округов, флотов, флотилий'."

        ./classloader -ng -s scen_load_cl_osoben_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования особенностей'.xml" & progress_load "Загрузка классификатора 'Наименования особенностей'."

        ./classloader -ng -s scen_load_cl_pd_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Пункты дислокации'.xml" & progress_load "Загрузка классификатора 'Пункты дислокации'."

        ./classloader -ng -s scen_load_cl_posts_gp_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_load "Загрузка классификатора 'Штатных должностей гражданского персонала'."

        ./classloader -ng -s scen_load_cl_posts_off_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей офицеров'.xml" & progress_load "Загрузка классификатора 'Штатных должностей офицеров'."

        ./classloader -ng -s scen_load_cl_posts_sold_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей солдат'.xml" & progress_load "Загрузка классификатора 'Штатных должностей солдат'."

        ./classloader -ng -s scen_load_cl_ppv_gd_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_load "Загрузка классификатора 'Пунктов погрузки-выгрузки'."

        ./classloader -ng -s scen_load_cl_prednaz_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_load "Загрузка классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'."

        ./classloader -ng -s scen_load_cl_rod_vs_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_load "Загрузка классификатора 'Перечень родов войск, специальных войск и служб'."

        ./classloader -ng -s scen_load_cl_vid_vs_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_load "Загрузка классификатора 'Перечень видов Вооруженных Сил'."

        ./classloader -ng -s scen_load_cl_specialization_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования специализации'.xml" & progress_load "Загрузка классификатора 'Наименования специализации'."
        
        ./classloader -ng -s scen_load_cl_state_names_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_load "Загрузка классификатора 'Наименования штатов и структурных подразделений'."


    fi
fi



if (($regime_mode == 4)); then 
    array_of_load_scen_files=( "scen_load_cl_bg_level_test_uch.xml" "scen_load_cl_garnizon_test_uch.xml" "scen_load_cl_gs_test_uch.xml"  "scen_load_cl_mob_events_test_uch.xml" "scen_load_cl_okrug_flot_test_uch.xml"  "scen_load_cl_osoben_test_uch.xml"  "scen_load_cl_pd_test_uch.xml" "scen_load_cl_posts_gp_test_uch.xml" "scen_load_cl_posts_off_test_uch.xml" "scen_load_cl_posts_sold_test_uch.xml" "scen_load_cl_ppv_gd_test_uch.xml" "scen_load_cl_prednaz_test_uch.xml"  "scen_load_cl_rod_vs_test_uch.xml" "scen_load_cl_ship_types_test_uch.xml" "scen_load_cl_specialization_test_uch.xml" "scen_load_cl_state_names_test_uch.xml" "scen_load_cl_vid_vs_test_uch.xml" "scen_load_cl_vvt_types_test_uch.xml")


    host_name="db_host"
    user_name="db_user"
    password="db_pass"
    port="db_port"

    temporary="temp_file.temp" # создаётся временный файл, в который будут записываться данные

    for xml_file in ${array_of_load_scen_files[@]}; do

        xmlstarlet ed -L -u "/scenario/db_host" -v $host_address_name $xml_file #host
        xmlstarlet ed -L -u "/scenario/db_name" -v $base_user_name $xml_file  #user

    done


    # Сохранение пользователем файлов классификаторов в формате ИМОД в директорию и подтверждение начала загрузки


    zenity --question --width=300 --height=120 \
        --text="Начать загрузку классификаторов в формате ИМОД в Базу данных sodo_test_uch?" \
        --ok-label="Да" \
        --cancel-label="Нет"

    download_start=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

    if [[ $download_start -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
        exit

    else   # если подтвердил, начинается загрузка
        
        progress_load () {
            (
            echo "10"; sleep 0.5
            echo "50"; sleep 0.5
            echo "80"; sleep 0.5
            echo "100"; sleep 0.5
            ) | 
            zenity --width=400 --height=150 --progress \
                                --text="$1" \
                                --title="Процесс загрузки...." \
                                --percentage=0 \
                                --pulsate \
                                --auto-close
            }       
        
    # Старт загрузки классификаторов в БД 
        ./classloader -ng -s scen_load_cl_bg_level_test_uch.xml -d "Классификатор 'Степени боевой готовности ВФ ИГ'.xml" & progress_load "Загрузка Классификатора 'Степени боеготовности формирований Вооруженных Сил'"

        ./classloader -ng -s scen_load_cl_ship_types_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Типы кораблей'.xml" & progress_load "Загрузка классификатора 'Типы кораблей'."
        
        
        ./classloader -ng -s scen_load_cl_vvt_types_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Вооружение, военная техника и другие материальные средства Министерства обороны'.xml" & progress_load "Загрузка классификатора 'Вооружение, военная техника и другие материальные средства Министерства обороны'."
        
        ./classloader -ng -s scen_load_cl_garnizon_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень гарнизонов'.xml" & progress_load "Загрузка классификатора 'Перечень гарнизонов'."
        

        ./classloader -ng -s scen_load_cl_gs_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименований главных слов'.xml" & progress_load "Загрузка классификатора 'Наименований главных слов'."
        

        ./classloader -ng -s scen_load_cl_mob_events_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Мобилизационные мероприятия'.xml" & progress_load "Загрузка классификатора 'Мобилизационные мероприятия'."
        

        ./classloader -ng -s scen_load_cl_okrug_flot_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень военных округов, флотов, флотилий'.xml" & progress_load "Загрузка классификатора 'Перечень военных округов, флотов, флотилий'."
        

        ./classloader -ng -s scen_load_cl_osoben_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования особенностей'.xml" & progress_load "Загрузка классификатора 'Наименования особенностей'."
        

        ./classloader -ng -s scen_load_cl_pd_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Пункты дислокации'.xml" & progress_load "Загрузка классификатора 'Пункты дислокации'."
        

        ./classloader -ng -s scen_load_cl_posts_gp_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей гражданского персонала'.xml" & progress_load "Загрузка классификатора 'Штатных должностей гражданского персонала'."
        

        ./classloader -ng -s scen_load_cl_posts_off_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей офицеров'.xml" & progress_load "Загрузка классификатора 'Штатных должностей офицеров'."
        

        ./classloader -ng -s scen_load_cl_posts_sold_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Штатных должностей солдат'.xml" & progress_load "Загрузка классификатора 'Штатных должностей солдат'."
        
        
        ./classloader -ng -s scen_load_cl_ppv_gd_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Пунктов погрузки-выгрузки'.xml" & progress_load "Загрузка классификатора 'Пунктов погрузки-выгрузки'."
        

        ./classloader -ng -s scen_load_cl_prednaz_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень предназначений соединений, воинских частей, учреждений и организаций'.xml" & progress_load "Загрузка классификатора 'Перечень предназначений соединений, воинских частей, учреждений и организаций'."
        

        ./classloader -ng -s scen_load_cl_rod_vs_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень родов войск, специальных войск и служб'.xml" & progress_load "Загрузка классификатора 'Перечень родов войск, специальных войск и служб'."
        

        ./classloader -ng -s scen_load_cl_vid_vs_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Перечень видов Вооруженных Сил'.xml" & progress_load "Загрузка классификатора 'Перечень видов Вооруженных Сил'."
        

        ./classloader -ng -s scen_load_cl_specialization_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования специализации'.xml" & progress_load "Загрузка классификатора 'Наименования специализации'."
        

        ./classloader -ng -s scen_load_cl_state_names_test_uch.xml -d /tmp/"Классификаторы в формате ИМОД для загрузки в БД СОДО"/"Классификатор 'Наименования штатов и структурных подразделений'.xml" & progress_load "Загрузка классификатора 'Наименования штатов и структурных подразделений'."
        

    fi
fi





zenity --question --width=300 --height=120 \
--text="Классификаторы успешно загружены. Для перехода в главное меню нажмите Далее, для выхода из программы нажмите Выход." \
--ok-label="Далее" \
--cancel-label="Выход"

continue_choice=$? # внутри переменной ответ от пользователя(да(true т.е. 0) или нет(false т.е. 1))

if [[ $continue_choice -eq 1 ]]; then  # если пользователь не подтвердил начало загрузки
    exit

else
    cd /opt/rbt/vbd/LOADER; sh start.sh
fi
#-------------------------------------------------------------------------------------------------------------------------------------------------------------
